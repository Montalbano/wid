#https://hub.docker.com/_/node/
#To create container: docker build -t wid .
#To run the container: docker run -it --rm --name wid -p 9090:9090 -v /<your path>/data:/usr/src/wid/sources/data wid
# the -p option allows to bind the port of the host (1st port) to the port of the container (2nd port)

FROM node:5.11.1

ENV WID_SERVER_CONTEXT=/wid
ENV WID_SERVER_MAIL=direct:?name=mail.codelutin.com
ENV WID_SERVER_MAIL_FROM=no-reply@codelutin.com
EXPOSE 9090

RUN mkdir -p /usr/src
WORKDIR /usr/src

RUN git clone https://gitlab.nuiton.org/jruchaud/wid.git
WORKDIR /usr/src/wid

RUN npm install

CMD [ "npm", "start" ]
