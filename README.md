# WID

What I did ?

You can follow your time with tags system. For example you put "java dev" during 2 hours.

The timing never stops. Put your time after you do the task, the hobbit, ...

You can use a public instance https://wid.chorem.com.

## Screens

![main_screen](http://wid.chorem.com/assets/img/timeline.png)

## Timeline
* add time, put tag and tap enter
* show events (idle, push, ...)
* edit, edit old tags and you can add note
* use, use the same tag
* split, modify the time
* merge, create one big time with two times
* note, add and remove some complement information

## Reports
* create report
* show report on the timeline
* navigate on old reports
* copy tree report

## Tags
* test query research
* mass tags modification

## Configuration
* add/remove identities for push events
* add/remove application for push events
* synchronization on Timebundle project
* import jtimer
* import/export json

## Extension Chrome
* idle detection
