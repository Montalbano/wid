/* eslint strict: 0 */
// Needed in Node in order to use let
"use strict";

let assert = require("assert");
let helper = require("./TestHelper");
let sign = helper.sign;

let timeApi = require("../src/js/api/TimeApi");

describe("Time", function() {

    it("Add a time", function(done) {
        let token;
        sign("JackTime")
        .then((t) => {
            token = t;
            return timeApi.add({
                token,
                beginDateTime: Date.now(),
                endDateTime: Date.now(),
                tags: ["js", "dev", "wid"],
                note: "ma note"
            });
        }).then((r) => {
            assert(r);
            return timeApi.getAll({token});
        }).then((r) => {
            assert(r.rows.length);
            done();
        });
    });

    it("Fusion a time", function(done) {
        let begin = Date.now();
        let middle = begin + 2000;
        let end = middle + 3000;

        let token;
        sign("JackFusion")
        .then((t) => {
            token = t;
            return timeApi.add({
                token,
                beginDateTime: begin,
                endDateTime: middle,
                tags: ["js", "dev", "wid"]
            });
        }).then((r) => {
            assert(r);
            return timeApi.add({
                token,
                beginDateTime: middle,
                endDateTime: end,
                tags: ["js", "dev", "wid"]
            });
        }).then((r) => {
            assert(r);
            return timeApi.getAll({token});
        }).then((r) => {
            assert.equal(3, r.rows.length);
            done();
        });
    });

    it("Remove a time", function(done) {
        let now = Date.now();
        let token;

        sign("JackRemove")
        .then((t) => {
            token = t;
            return timeApi.add({
                token,
                beginDateTime: now,
                endDateTime: now,
                tags: ["js", "dev", "wid"]
            });
        }).then((r) => {
            assert(r);
            return timeApi.removeOnDate({token, beginDateTime: now, endDateTime: now});
        }).then(() => {
            return timeApi.getAll({token});
        }).then((r) => {
            assert(!r.rows.length);
            done();
        });
    });

    it("Modify a date", function(done) {
        let now = Date.now();
        let newDate = Date.now();
        let token;

        sign("JackModify")
        .then((t) => {
            token = t;
            return timeApi.add({
                token,
                beginDateTime: now,
                endDateTime: now,
                tags: ["js", "dev", "wid"]
            });
        }).then((r) => {
            assert(r);
            return timeApi.modifyDate({
                token,
                oldBeginDateTime: now,
                oldEndDateTime: now,
                newBeginDateTime: newDate,
                newEndDateTime: newDate
            });
        }).then(() => {
            return timeApi.getAll({token});
        }).then((r) => {
            assert(r.rows.length);
            done();
        });
    });

    it("Merge up", function(done) {
        let begin = Date.now();
        let end = Date.now();
        let next = Date.now();
        let token;

        sign("JackMergeUp")
        .then((t) => {
            token = t;
            return timeApi.add({
                token,
                beginDateTime: begin,
                endDateTime: end,
                tags: ["js"]
            });
        }).then((r) => {
            assert(r);
            return timeApi.add({
                token,
                beginDateTime: end,
                endDateTime: next,
                tags: ["dev", "wid"]
            });
        }).then((r) => {
            assert(r);
            return timeApi.mergeUp({
                token,
                oldBeginDateTime: end,
                oldEndDateTime: next
            });
        }).then(() => {
            return timeApi.getAll({token});
        }).then((r) => {
            assert(r.rows.length);
            r.rows.forEach((value) => {
                assert.equal(value.beginDateTime, begin);
                assert.equal(value.endDateTime, next);
            });
            done();
        });
    });

    it("Merge down", function(done) {
        let begin = Date.now();
        let end = Date.now();
        let next = Date.now();
        let token;

        sign("JackMergeDown")
        .then((t) => {
            token = t;
            return timeApi.add({
                token,
                beginDateTime: begin,
                endDateTime: end,
                tags: ["js"]
            });
        }).then((r) => {
            assert(r);
            return timeApi.add({
                token,
                beginDateTime: end,
                endDateTime: next,
                tags: ["dev", "wid"]
            });
        }).then((r) => {
            assert(r);
            return timeApi.mergeDown({
                token,
                oldBeginDateTime: begin,
                oldEndDateTime: end
            });
        }).then(() => {
            return timeApi.getAll({token});
        }).then((r) => {
            assert(r.rows.length);
            r.rows.forEach((value) => {
                assert.equal(value.beginDateTime, begin);
                assert.equal(value.endDateTime, next);
            });
            done();
        });
    });

    it("Report", function(done) {
        let now = Date.now();
        let begin = now;
        let end = now + 20000;
        let next = end + 30000;
        let token;

        sign("JackReport")
        .then((t) => {
            token = t;
            return timeApi.add({
                token,
                beginDateTime: begin,
                endDateTime: end,
                tags: ["js", "wid"]
            });
        }).then((r) => {
            assert(r);
            return timeApi.add({
                token,
                beginDateTime: end,
                endDateTime: next,
                tags: ["dev", "wid"]
            });
        }).then((r) => {
            assert(r);
            return timeApi.getTotalTimeForReport({
                token,
                oldBeginDateTime: begin,
                oldEndDateTime: next,
                query: "wid"
            });
        }).then((v) => {
            assert.equal(50000, v.time);
        }).then(() => {
            return timeApi.getTotalTimeForReport({
                token,
                oldBeginDateTime: begin,
                oldEndDateTime: next,
                query: "wid+dev"
            });
        }).then((v) => {
            assert.equal(30000, v.time);
        }).then(() => {
            return timeApi.getTotalTimeForReport({
                token,
                oldBeginDateTime: begin,
                oldEndDateTime: next,
                query: "wid js"
            });
        }).then((v) => {
            assert.equal(50000, v.time);
            done();
        });
    });

    it("get begin date", function(done) {
        let now = Date.now();
        let token;

        sign("JackBeginDate")
        .then((t) => {
            token = t;
            return timeApi.add({
                token,
                beginDateTime: now,
                endDateTime: now,
                tags: ["js", "dev", "wid"]
            });
        }).then(() => {
            return timeApi.getBeginDateTime({token});
        }).then((r) => {
            assert(r.beginDateTime);
            done();
        });
    });

    it("no begin date", function(done) {
        sign("JackNoBeginDate")
        .then((t) => {
            return timeApi.getBeginDateTime({token: t});
        }).then((r) => {
            assert.equal(null, r.beginDateTime);
            done();
        });
    });

    it("Facet", function(done) {
        let now = Date.now();
        let begin = now;
        let end = now + 20000;
        let next = end + 30000;
        let token;

        sign("JackFacet")
        .then((t) => {
            token = t;
            return timeApi.add({
                token,
                beginDateTime: begin,
                endDateTime: end,
                tags: ["js", "wid", "facet", "test"]
            });
        }).then((r) => {
            assert(r);
            return timeApi.add({
                token,
                beginDateTime: end,
                endDateTime: next,
                tags: ["dev", "wid", "facet"]
            });
        }).then((r) => {
            assert(r);
            return timeApi.facet({
                token
            });
        }).then((v) => {
            assert(v.rows.length);
            done();
        });
    });

    it("Start with", function(done) {
        let now = Date.now();
        let token;

        sign("JackStartWith")
        .then((t) => {
            token = t;
            return timeApi.add({
                token,
                beginDateTime: now,
                endDateTime: now,
                tags: ["js", "dev", "dudu"]
            });
        }).then(() => {
            return timeApi.getStartWith({token, tag: ["d"]});
        }).then((r) => {
            assert.equal(2, r.rows.length);
            done();
        });
    });

    it("Completion", function(done) {
        let now = Date.now();
        let begin = now;
        let end = now + 20000;
        let next = end + 30000;
        let token;

        sign("JackCompletion")
        .then((t) => {
            token = t;
            return timeApi.add({
                token,
                beginDateTime: begin,
                endDateTime: end,
                tags: ["js", "wid", "facet", "test"]
            });
        }).then((r) => {
            assert(r);
            return timeApi.add({
                token,
                beginDateTime: end,
                endDateTime: next,
                tags: ["dev", "wid", "facet"]
            });
        }).then((r) => {
            assert(r);
            return timeApi.completion({
                token,
                tags: ["f"]
            });
        }).then((v) => {
            assert(v.rows.length);
            done();
        });
    });
});
