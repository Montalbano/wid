/* eslint strict: 0 */
// Needed in Node in order to use let
"use strict";

let fs = require("fs");
let assert = require("assert");
let helper = require("./TestHelper");
let sign = helper.sign;

let importApi = require("../src/js/api/ImportApi");
let userApi = require("../src/js/api/UserApi");

describe("Import", function() {
    // this.timeout(0);

    it("Jtimer", function(done) {
        let file = fs.readFileSync("./test/importJTimerTestData.zip");
        // console.log(file.buffer);

        sign("JackJtimer")
        .then((t) => {
            return importApi.importJtimer({token: t, data: file});
        })
        .then((r) => {
            assert(r);
            done();
        });
    });

    it("Database", function(done) {
        let token;
        let file = fs.readFileSync("./test/importJTimerTestData.zip");
        // console.log(file.buffer);

        sign("JackDatabase")
        .then((t) => {
            token = t;
            return importApi.importJtimer({token, data: file});
        })
        .then((r) => {
            assert(r);
            return userApi.exportDb({token});
        })
        .then((r) => {
            assert(r.file);
            return importApi.importDb({token, data: r.file, drop: true});
        })
        .then((r) => {
            assert(r);
            done();
        });
    });
});
