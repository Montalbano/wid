/* eslint strict: 0 */
// Needed in Node in order to use let
"use strict";

global.dev = true;

let userApi = require("../src/js/api/UserApi");

module.exports.sign = (name) => {
    return userApi.signUp({
        username: name,
        email: name + "@",
        passwd: name
    })
    .then(userApi.signIn.bind(null, {username: name, passwd: name}))
    .then(({token}) => {
        return token;
    });
};
