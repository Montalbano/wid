/* eslint strict: 0 */
// Needed in Node in order to use let
"use strict";

let assert = require("assert");
let helper = require("./TestHelper");
let sign = helper.sign;

let eventApi = require("../src/js/api/EventApi");

describe("Event", function() {

    it("Add", function(done) {
        let begin = Date.now();
        let end = begin + 20000;
        let token;

        sign("JackAddEvent")
        .then((t) => {
            token = t;
            return eventApi.add({
                token,
                beginDateTime: begin,
                endDateTime: end,
                source: "test",
                value: "test"
            });
        }).then((v) => {
            assert(v);
            return eventApi.getAll({token, beginDateTime: begin});
        }).then((r) => {
            assert(r.rows.length);
            done();
        });
    });

    it("AddAll", function(done) {
        let begin = Date.now();
        let end = begin + 20000;
        let token;

        sign("JackAddAllEvent")
        .then((t) => {
            token = t;
            return eventApi.addAll({
                token,
                events: [{
                    beginDateTime: begin,
                    endDateTime: end,
                    source: "test",
                    value: "test"
                },
                {
                    beginDateTime: begin,
                    endDateTime: end,
                    source: "test2",
                    value: "test2"
                }]
            });
        }).then((v) => {
            assert(v);
            return eventApi.getAll({token, beginDateTime: begin});
        }).then((r) => {
            assert(r.rows.length);
            done();
        });
    });

    it("Remove", function(done) {
        let begin = Date.now();
        let end = begin + 20000;
        let token;

        sign("JackRemoveEvent")
        .then((t) => {
            token = t;
            return eventApi.add({
                token,
                beginDateTime: begin,
                endDateTime: end,
                source: "test",
                value: "test"
            });
        }).then((v) => {
            assert(v);
            return eventApi.remove({token, id: v.id});
        }).then((v) => {
            assert(v);
            return eventApi.getAll({token, beginDateTime: begin});
        }).then((r) => {
            assert(!r.rows.length);
            done();
        });
    });

    it("By source", function(done) {
        let begin = Date.now();
        let end = begin + 20000;
        let token;

        sign("JackSource")
        .then((t) => {
            token = t;
            return eventApi.add({
                token,
                beginDateTime: begin,
                endDateTime: end,
                source: "test",
                value: "test"
            });
        }).then((v) => {
            assert(v);
            return eventApi.add({
                token,
                beginDateTime: begin,
                endDateTime: end,
                source: "other",
                value: "test"
            });
        }).then((v) => {
            assert(v);
            return eventApi.getBySource({token, source: "test"});
        }).then((r) => {
            assert.equal(1, r.rows.length);
            done();
        });
    });

    it("Finish", function(done) {
        let begin = Date.now();
        let token;

        sign("JackFinishEvent")
        .then((t) => {
            token = t;
            return eventApi.finish({
                token,
                source: "test",
                value: "test"
            });
        }).then((v) => {
            assert(v);
            return eventApi.getAll({token, beginDateTime: begin});
        }).then((r) => {
            assert(r.rows.length);
            done();
        });
    });

    it("Start then end", function(done) {
        let begin = Date.now();
        let end = begin + 20000;
        let token;

        sign("JackStartEndEvent")
        .then((t) => {
            token = t;
            return eventApi.start({
                token,
                beginDateTime: begin,
                source: "test",
                value: "test"
            });
        }).then((v) => {
            assert(v);
            return eventApi.end({
                token,
                endDateTime: end,
                source: "test"
            });
        }).then((v) => {
            assert(v);
            return eventApi.getAll({token, beginDateTime: begin});
        }).then((r) => {
            assert.equal(1, r.rows.length);
            assert.equal(begin, r.rows[0].beginDateTime);
            assert.equal(end, r.rows[0].endDateTime);
            done();
        });
    });
});
