/* eslint strict: 0 */
/* eslint new-cap: 0 */
/* eslint no-console: 0 */
/* eslint no-process-env: 0 */

// Needed in Node in order to use let
"use strict";

let express = require("express");
let webpack = require("webpack");
let webpackMiddleware = require("webpack-dev-middleware");
let webpackHotMiddleware = require("webpack-hot-middleware");
let config = require("./webpack.config.js");
let cookie = require("cookie");
let cookieParser = require("cookie-parser");
let bodyParser = require("body-parser");

let multer = require("multer");
let upload = multer({path: "/tmp"});

let dbHelper = require("./src/js/api/DatabaseHelper");
let UnauthorizedError = require("./src/js/api/UnauthorizedError");

let serverContext = process.env.WID_SERVER_CONTEXT || "";
let port = process.env.WID_SERVER_PORT || 9090;

let app = express();
let router = express.Router();

let http = require("http").Server(app);
let io = require("socket.io")(http, {path: serverContext + "/ws"});
io.listen(http);

router.use(cookieParser());
// parse application/x-www-form-urlencoded
router.use(bodyParser.urlencoded({extended: false}));
// parse application/json
router.use(bodyParser.json());

// Wrapper use to call the API and return the result in the response
let callApi = (call, done) => {
    return (req, res) => {
        let params = Object.assign({}, req.query, req.cookies);

        if (req.file) {
            params[req.file.fieldname] = req.file.buffer;
        }

        call(params)
        .then((result) => {
            if (done) {
                done(req, res, result);
            } else {
                res.json(result);
            }
        })
        .catch((err) => {
            console.error(req.originalUrl, err);
            if (err instanceof UnauthorizedError) {
                res.status(401).send(JSON.stringify({err: err.msg || "Server error"}));
            } else {
                res.status(500).send(JSON.stringify({err: err || "Server error"}));
            }
        });
    };
};

// USER API
let userApi = require("./src/js/api/UserApi");

router.get("/api/user", callApi(userApi.getUser));
router.get("/api/signup", callApi(userApi.signUp));
router.get("/api/signin", callApi(userApi.signIn, (req, res, user) => {
    res.cookie("token", user.token, {httpOnly: true});
    res.json(user);
}));
router.get("/api/signout", callApi(userApi.signOut));
router.get("/api/export", callApi(userApi.exportDb, (req, res, result) => {
    res.header("Content-Disposition", "attachment; filename=\"" + result.name + ".json\"");
    res.send(result.file);
}));
router.get("/api/password/reset", callApi(userApi.generatePassword));
router.get("/api/password/change", callApi(userApi.changePassword));

// TIME API
let timeApi = require("./src/js/api/TimeApi");

router.get("/api/times", callApi(timeApi.getAll));
router.get("/api/time/add", callApi(timeApi.add));
router.get("/api/time/save", callApi(timeApi.createOrUpdate));
router.get("/api/time/modify", callApi(timeApi.modifyDate));
router.get("/api/time/merge/up", callApi(timeApi.mergeUp));
router.get("/api/time/merge/down", callApi(timeApi.mergeDown));
router.get("/api/time/facet", callApi(timeApi.facet));
router.get("/api/time/facet/export", callApi(timeApi.facetExport));
router.get("/api/time/completion", callApi(timeApi.completion));
router.get("/api/time/tags", callApi(timeApi.getTags));
router.get("/api/time/tags/update", callApi(timeApi.updateTags));

// IMPORT API
let importApi = require("./src/js/api/ImportApi");

router.post("/api/import/jtimer", upload.single("data"), callApi(importApi.importJtimer));
router.post("/api/import/json", upload.single("data"), callApi(importApi.importDb));

// REPORT API
let reportApi = require("./src/js/api/ReportApi");

router.get("/api/reports", callApi(reportApi.getAll));
router.get("/api/report/add", callApi(reportApi.add));
router.get("/api/report/remove", callApi(reportApi.remove));
router.get("/api/report/total", callApi(reportApi.getTotalTime));
router.get("/api/report/widgets", callApi(reportApi.getWidgetsTime));
router.get("/api/report/details", callApi(reportApi.getDetailsTime));

// EVENT API
let eventApi = require("./src/js/api/EventApi");

let eventUpdate = (req, res, result) => {
    let token = req.cookies.token;
    let name = dbHelper.getName(token);
    io.to(name).emit("eventUpdate", [result]);
    res.json(result);
};

router.get("/api/events", callApi(eventApi.getAll));
router.get("/api/event/add", callApi(eventApi.add, eventUpdate));
router.get("/api/event/start", callApi(eventApi.start, eventUpdate));
router.get("/api/event/end", callApi(eventApi.end, eventUpdate));
router.get("/api/event/remove", callApi(eventApi.remove));

// TIMEBUNDLE API
let timebundleApi = require("./src/js/api/TimebundleApi");

router.get("/api/timebundle/add", callApi(timebundleApi.add));
router.get("/api/timebundle/remove", callApi(timebundleApi.remove));
router.get("/api/timebundle/save", callApi(timebundleApi.save));
router.get("/api/timebundle/projects", callApi(timebundleApi.getProjects));
router.get("/api/timebundle/tasks", callApi(timebundleApi.getTasks));
router.get("/api/timebundle/sync", callApi(timebundleApi.sync));

// WEB SOCKET
io.on("connection", function(socket) {
    let token = cookie.parse(socket.client.request.headers.cookie).token;
    let name = dbHelper.getName(token);
    socket.join(name);
});

// Application
let applicationApi = require("./src/js/api/ApplicationApi");

router.get("/api/keys", callApi(applicationApi.getKeys));
router.get("/api/key/add", callApi(applicationApi.createKey));
router.get("/api/key/remove", callApi(applicationApi.removeKey));

router.get("/api/identities", callApi(applicationApi.getIdentities));
router.get("/api/identity/add", callApi(applicationApi.createIdentity));
router.get("/api/identity/remove", callApi(applicationApi.removeIdentity));

// ssh -R *:9643:localhost:9090 maven-release@goh.codelutin.com
router.post("/webhook/:key", (req, res) => {
    let body = req.body;
    let key = req.params.key;

    let hook;
    if (req.header("X-Gitlab-Event")) {
        hook = applicationApi.onGitLabWebHook;
    } else if (req.header("X-GitHub-Event")) {
        hook = applicationApi.onGitHubWebHook;
    } else {
        hook = applicationApi.onEventsWebHook;
    }

    hook({key, data: body})
    .then((result) => {
        if (result) {
            let name = dbHelper.getName(result.token);
            io.to(name).emit("eventUpdate", result.events);
        }
        res.sendStatus(200);
    })
    .catch((err) => {
        console.error(req.originalUrl, err);
        res.status(500).send(JSON.stringify({err: err || "Server error"}));
    });
});

router.use("/assets", express.static(__dirname + "/src/assets"));
router.get("/wid-ext.crx", function(req, res) {
    res.sendFile(__dirname + "/dist/wid-ext.crx");
});

let compiler = webpack(config);

let middleware = webpackMiddleware(compiler, {
    publicPath: config.output.publicPath,
    contentBase: "src",
    stats: {
        chunks: false,
        colors: true,
        progress: true
    }
});

router.use(middleware);
router.use(webpackHotMiddleware(compiler));

router.use("/*", middleware);
router.use("/*", webpackHotMiddleware(compiler));

// Start the server
// app.listen(9090, "0.0.0.0");
app.use(serverContext, router);

http.listen(port);
console.log(`Server start on port ${port}`);
