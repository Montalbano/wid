import React from "react";

import TimelineStore from "../stores/TimelineStore";
import {changeFilter} from "../actions/TimelineActions";

function getStateFromStores() {
    return {
        filter: TimelineStore.getFilter()
    };
}

class Filter extends React.Component {

    constructor() {
        super();
        this.state = getStateFromStores();
        this._onChange = this._onChange.bind(this);
    }

    componentDidMount() {
        TimelineStore.addChangeListener(this._onChange);
    }

    componentWillUnmount() {
        TimelineStore.removeChangeListener(this._onChange);
    }

    _onChange() {
        this.setState(getStateFromStores());
    }

    _onSelect(value) {
        this.setState({
            filter: value
        });
        changeFilter(value);
    }

    render() {
        return (
            <div className="filter">
                <div className="filter-type">
                    <div className={this.state.filter === 0 ? "selected" : ""} onClick={this._onSelect.bind(this, 0)}>Today</div>
                    <div className={this.state.filter === 1 ? "selected" : ""} onClick={this._onSelect.bind(this, 1)}>Week</div>
                    <div className={this.state.filter === 2 ? "selected" : ""} onClick={this._onSelect.bind(this, 2)}>Month</div>
                    <div className={this.state.filter === 3 ? "selected" : ""} onClick={this._onSelect.bind(this, 3)}>Year</div>
                </div>
            </div>
        );
    }
}

Filter.propTypes = {
};

Filter.contextTypes = {
};

export default Filter;
