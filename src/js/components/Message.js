import React from "react";
import MessageStore from "../stores/MessageStore";
import {clearMessage} from "../actions/MessageActions";

function getStateFromStores() {
    return {
        message: MessageStore.getMessage(),
        type: MessageStore.getType()
    };
}

class Message extends React.Component {

    constructor() {
        super();

        this.state = getStateFromStores();
        this._onMessage = this._onMessage.bind(this);
        this._onClose = this._onClose.bind(this);
    }

    componentDidMount() {
        MessageStore.addChangeListener(this._onMessage);
    }

    componentWillUnmount() {
        MessageStore.removeChangeListener(this._onMessage);
    }

    _onMessage() {
        this.setState(getStateFromStores());
    }

    _onClose() {
        clearTimeout(this.timer);
        clearMessage();
    }

    render() {
        let content = this.state.message;

        let className = "";
        if (content) {
            className = "show";
            this.timer = setTimeout(clearMessage, 15 * 1000);

        } else if (this.timer) {
            className = "hide";
            this.timer = null;
        }

        return (
            <div className={"message " + className}>{content}<span className="message-close" onClick={this._onClose}>x</span></div>
        );
    }
}

Message.propTypes = {
};

Message.contextTypes = {
};

export default Message;
