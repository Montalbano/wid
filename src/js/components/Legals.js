import React from "react";

class Legals extends React.Component {

    render() {
        return (
            <div className="legals">
                <h1>Mentions légales</h1>

                <h2>Société</h2>

                <h3>Code Lutin</h3>
                <p>Société par actions simplifiée au capital de 785,00 €.</p>
                <p>N° Siret&nbsp;: 44211670300046</p>

                <h3>Siège social</h3>
                <p>SAS Code Lutin<br/>
                12 Avenue Jules Verne<br/>
                44230 Saint-Sébastien-Sur-Loire</p>
                <p>Pour nous contacter&nbsp;: <a href="mailto:webadmin@codelutin.com">e-mail</a></p>

                <h3>Directeur de la publication</h3>
                <p>M Arnaud THIMEL, Président</p>

                <h2>Hébergeur</h2>
                <p>OVH<br/>
                Siège social : 2 rue Kellermann - 59100 Roubaix - France.<br/>
                Téléphone : +33 (0)8 203 203 63
                </p>

            <h2>Informatique et libertés</h2>

                <h3>Informations personnelles collectées</h3>
                <p>En France, les données personnelles sont notamment protégées par la
                loi n°&nbsp;78-87 du 6&nbsp;janvier 1978, la loi n°&nbsp;2004-801 du 6&nbsp;août 2004,
                l’article L.&nbsp;226-13 du Code pénal et la Directive Européenne du 24
                &nbsp;octobre 1995.</p>
                <p>En tout état de cause Code Lutin ne collecte des informations
                personnelles relatives à l’utilisateur (nom, adresse électronique,
                coordonnées téléphoniques) que pour le besoin des services proposés par
                les sites du réseau Code Lutin, notamment pour l’inscription à des
                espaces de discussion par le biais de formulaires en ligne ou pour des
                traitements statistiques. L’utilisateur fournit ces informations en
                toute connaissance de cause, notamment lorsqu’il procède par lui-même à
                leur saisie. Il est alors précisé à l’utilisateur des sites du réseau
                Code Lutin le caractère obligatoire ou non des informations qu’il serait
                amené à fournir.</p>

                <h3>Analyse statistique et confidentialité</h3>
                <p>En vue d’adapter le site aux demandes de nos visiteurs, nous analysons le trafic de nos sites avec Piwik.
                Piwik génère un cookie avec un identifiant unique, dont la durée de conservation est limitée à 13 mois.
                Les données recueillies (adresse IP, User-Agent…) sont anonymisées et conservées pour une durée de 6 mois.
                Elles ne sont pas cédées à des tiers ni utilisées à d’autres fins.</p>

                <h3>Rectification des informations nominatives collectées</h3>
                <p>Conformément aux dispositions de l’article&nbsp;34 de la loi n°&nbsp;48-87 du
                6 janvier 1978, l’utilisateur dispose d’un droit de modification des
                données nominatives collectées le concernant. Pour ce faire,
                l’utilisateur envoie à Code Lutin&nbsp;: </p>
                <ul>
                    <li>un courrier électronique </li>
                    <li>un courrier à l’adresse du siège de la société (indiquée
                    ci-dessus) en indiquant son nom ou sa raison sociale, ses coordonnées
                    physiques et/ou électroniques, ainsi que le cas échéant la référence
                    dont il disposerait en tant qu’utilisateur du site.</li>
                </ul>
                <p>La modification interviendra dans des délais raisonnables à compter
                de la réception de la demande de l’utilisateur.</p>

            <h2>Limitation de responsabilité</h2>
                <p>Ce site comporte des informations mises à disposition par des
                communautés ou sociétés externes ou des liens hypertextes vers d’autres sites qui
                n’ont pas été développés par Code Lutin. Le contenu mis à disposition
                sur le site est fourni à titre informatif. L’existence d’un lien de ce
                site vers un autre site ne constitue pas une validation de ce site ou
                de son contenu. Il appartient à l’internaute d’utiliser ces
                informations avec discernement et esprit critique. La responsabilité de
                Code Lutin ne saurait être engagée du fait des informations, opinions et
                recommandations formulées par des tiers.</p>
                <p>Code Lutin ne pourra être tenue responsable des dommages directs et
                indirects causés au matériel de l’utilisateur, lors de l’accès au site,
                 et résultant soit de l’utilisation d’un matériel ne répondant pas aux
                spécifications techniques requises, soit de l’apparition d’un bug ou
                d’une incompatibilité.</p>
                <p>Code Lutin ne pourra également être tenue responsable des dommages
                indirects (tels par exemple qu’une perte de marché ou perte d’une
                chance) consécutifs à l’utilisation du site.</p>
                <p>Code Lutin se réserve également la possibilité de mettre en
                cause la responsabilité civile et/ou pénale de l’utilisateur, notamment
                en cas de message à caractère raciste, injurieux, diffamant, ou
                pornographique, quel que soit le support utilisé (texte, photographie…).</p>

                <h3>Limitations contractuelles sur les données techniques</h3>
                <p>Code Lutin ne pourra être tenue responsable de dommages matériels
                liés à l’utilisation du site. De plus, l’utilisateur du site s’engage à
                accéder au site en utilisant un matériel récent, ne contenant pas de
                virus et avec un navigateur de dernière génération mis-à-jour</p>

                <h3>Propriété intellectuelle</h3>
                <p>Les contenus sont publiés sous la responsabilité des utilisateurs.</p>

                <h1>Licence</h1>
                <p>
                    Les Conditions Générales d’Utilisation du service sont placés sous
                    <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.fr"> Creative Commons By-SA</a>. Elles ont été inspirées par les CGU de Framasoft.
                </p>
            </div>
        );
    }
}

export default Legals;
