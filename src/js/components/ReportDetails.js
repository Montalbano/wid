/* eslint no-alert: 0*/
import React from "react";
import ReportStore from "../stores/ReportStore";
import {getReports, getDetails} from "../actions/ReportActions";
import {diffValue, getDateTimeForPeriod} from "../utils/DateTimeUtils";
import moment from "moment";
import ReportCircle from "./ReportCircle";
import ReportHistory from "./ReportHistory";
import wordcloud from "wordcloud";
import clipboard from "clipboard-js";

function getStateFromStores({id, date}) {
    let report = ReportStore.getReport(id);
    if (report) {
        date = getDateTimeForPeriod(report.period, date).beginDateTime;
    }

    return {
        id,
        report,
        date: +date,
        detail: ReportStore.getDetails(id, date)
    };
}

function getEmpty({id}) {
    return {
        id,
        report: {
            name: "",
            period: 0,
            tags: []
        },
        date: +moment(),
        detail: {
            details: [],
            history: [],
            facets: [],
            notes: []
        }
    };
}

function sortTags(tags) {
    return tags.sort().join(" ");
}

function formatPeriod(period, dateTime) {
    let date = moment(dateTime);

    let format;
    if (period === 0) {
        date.startOf("day");
        format = "L";

    } else if (period === 1) {
        date.startOf("week");
        format = "w";

    } else if (period === 2) {
        date.startOf("month");
        format = "MMMM";

    } else if (period === 3) {
        date.startOf("year");
        format = "gggg";
    }

    return date.format(format);
}

class ReportDetails extends React.Component {

    constructor({params}) {
        super();
        this.state = getEmpty(params);

        this.state.displayNote = true;
        this.state.displayTotal = true;

        this._onChange = this._onChange.bind(this);
        this._changeDate = this._changeDate.bind(this);
        this._setTag = this._setTag.bind(this);
        this._copy = this._copy.bind(this);
        this._toggleNote = this._toggleNote.bind(this);
        this._toggleCount = this._toggleCount.bind(this);
        this._toggleTotal = this._toggleTotal.bind(this);
        this._togglePercentage = this._togglePercentage.bind(this);
    }

    componentDidMount() {
        getReports();
        ReportStore.addChangeListener(this._onChange);
    }

    componentWillReceiveProps({params}) {
        this.setState(getStateFromStores(params), () => {
            this._getDetails();
        });
    }

    componentWillUnmount() {
        ReportStore.removeChangeListener(this._onChange);
    }

    _getDetails() {
        let {period} = this.state.report;
        let {beginDateTime, endDateTime} = getDateTimeForPeriod(period, this.state.date);

        getDetails(this.state.id, beginDateTime, endDateTime);
    }

    _onChange() {
        let {id, date} = this.props.params;

        if (!this.state.report.id) {
            this.state = Object.assign(this.state, getStateFromStores({id, date}));
            this._getDetails();
            return;
        }
        this.setState(Object.assign(this.state, getStateFromStores({id, date})));

        let {period} = this.state.report;

        let weightFactor = 1;
        if (period === 0) {
            weightFactor = 20;
        } else if (period === 1) {
            weightFactor = 10;
        } else if (period === 2) {
            weightFactor = 5;
        } else if (period === 3) {
            weightFactor = 1;
        }

        let width = this.refs.cloudContainer.offsetWidth;
        this.refs.cloud.width = width;

        wordcloud(this.refs.cloud, {
            gridSize: 18,
            weightFactor,
            list: this._getList(),
            color: function() {
                return (["#AE69AF", "#47BAC1", "#15A4FA"])[Math.floor(Math.random() * 3)];
            },
            backgroundColor: "#ffffff"
        });
    }

    _changeDate(date) {
        this.context.router.push(`/report/${this.state.id}/${date}`);
    }

    _setTag(tags) {
        this.setState(Object.assign(this.state, {
            tags
        }));
    }

    _toggleNote() {
        let displayNote = this.refs.note.checked ? 1 : 0;
        this.setState(Object.assign(this.state, {
            displayNote
        }));
    }

    _toggleTotal() {
        let displayTotal = this.refs.total.checked ? 1 : 0;
        this.setState(Object.assign(this.state, {
            displayTotal
        }));
    }

    _toggleCount() {
        let displayCount = this.refs.count.checked ? 1 : 0;
        this.setState(Object.assign(this.state, {
            displayCount
        }));
    }

    _togglePercentage() {
        let displayPercentage = this.refs.percentage.checked ? 1 : 0;
        this.setState(Object.assign(this.state, {
            displayPercentage
        }));
    }

    _getList() {
        let {details} = this.state.detail;
        let list = new Map();

        details.forEach((value) => {
            let tags = value.tags.split(" ");

            tags.forEach((tag) => {
                let count = list.get(tag) || 0;
                list.set(tag, ++count);
            });
        });

        return Array.from(list.entries());
    }

    _copy() {
        clipboard.copy(this.reportExport);
        window.alert("Copied into clipboard");
    }

    extractNotes() {
        let {details, notes} = this.state.detail;

        let notesMap = new Map();
        let notesReversed = notes.slice(0).reverse();
        let noteIndex = 0;
        details.forEach((value) => {
            let note = notesReversed[noteIndex];

            while (noteIndex < notes.length
                    && note.beginDateTime < value.endDateTime
                    && note.endDateTime >= value.beginDateTime) {

                let key = sortTags(value.tags.split(" "));
                let array = notesMap.get(key);
                if (!array) {
                    array = [];
                    notesMap.set(key, array);
                }
                array.push(note.value);

                noteIndex++;
                note = notesReversed[noteIndex];
            }
        });

        return notesMap;
    }

    renderFacets(facets, notesMap, tagsList = []) {
        let keys = Object.keys(facets);

        let separator = "  ".repeat(tagsList.length);
        let items = keys.map((key) => {
            if (key !== "__meta__") {
                let {tag, duration, count} = facets[key].__meta__;

                let tags = tagsList.concat(tag);
                let tagsKey = sortTags(tags);
                let notes = notesMap.get(tagsKey);

                let totalInfo = "";
                if (this.state.displayTotal) {
                    totalInfo = " - " + diffValue(duration);
                }

                let countInfo = "";
                if (this.state.displayCount) {
                    countInfo = " (" + count + ")";
                }

                let percentageInfo = "";
                if (this.state.displayPercentage) {
                    let {total} = this.state.detail;
                    let percentageValue = (duration / total) * 100;
                    percentageInfo = " - " + percentageValue.toFixed(2) + "%";
                }

                this.reportExport += separator + "- " + tag + countInfo + totalInfo + percentageInfo + "\n";

                let notesContent;
                if (this.state.displayNote) {
                    notesContent = notes && notes.map((note, index) => {
                        this.reportExport += separator + "    * " + note + "\n";
                        return <li key={index}>{note}</li>;
                    });
                }

                return (
                    <li key={key}>
                        <a className="action" key={tag} onClick={this._setTag.bind(this, tags)}>{tag}</a>
                        {countInfo}{totalInfo}{percentageInfo}
                        {this.renderFacets(facets[key], notesMap, tags)}
                        <ul>{notesContent}</ul>
                    </li>
                );
            }
            return "";
        });

        return (<ul>{items}</ul>);
    }

    render() {
        let {details, history, total} = this.state.detail;
        let {name, period} = this.state.report;

        let periodFormat = formatPeriod(period, this.state.date);

        // Notes
        let notesMap = this.extractNotes();
        this.reportExport = name + " - " + periodFormat + ":\n";
        let facetsContent = this.renderFacets(this.state.detail.facets, notesMap);

        // Details
        let filter;
        if (this.state.tags && this.state.tags.length) {
            filter = new RegExp("(^| )((" + this.state.tags.join(")|(") + "))(?=( |$))", "g");
        }

        let detailsContent = details.filter((value) => {
            let match = value.tags.match(filter);
            return !filter || match && match.length >= this.state.tags.length;
        })
        .map((value, index) => {
            return (
                <tr className="main-item" key={index}>
                    <td>{moment(value.beginDateTime).format("lll")}</td>
                    <td>{moment(value.endDateTime).format("lll")}</td>
                    <td>{value.tags}</td>
                    <td>{diffValue(value.duration)}</td>
                </tr>
            );
        });

        return (
            <div className="reports main">
                <div className="main-table report-circle--2x">

                    <div className="main-separator">Report {name} - {periodFormat}</div>
                    <div className="report-stats">
                        <ReportCircle className={"report-circle--color2"} widget={{
                            name: periodFormat,
                            period,
                            time: total
                        }}/>
                        <ReportHistory date={this.state.date} onClick={this._changeDate} period={period} values={history}/>
                    </div>

                    <div className="main-separator">Summary</div>
                    <div className="main-body" ref="cloudContainer">
                        <canvas height="300px" ref="cloud"></canvas>
                    </div>

                    <div className="main-separator">Times</div>
                    <div className="main-body report-times">
                        <input checked={this.state.displayTotal} id="total" onChange={this._toggleTotal} ref="total" type="checkbox" value="true"/>
                        <label htmlFor="total">Total</label>

                        <input checked={this.state.displayCount} id="count" onChange={this._toggleCount} ref="count" type="checkbox" value="true"/>
                        <label htmlFor="count">Count</label>

                        <input checked={this.state.displayNote} id="note" onChange={this._toggleNote} ref="note" type="checkbox" value="true"/>
                        <label htmlFor="note">Note</label>

                        <input checked={this.state.displayPercentage} id="percentage" onChange={this._togglePercentage} ref="percentage" type="checkbox" value="true"/>
                        <label htmlFor="percentage">Percentage</label>

                        <ul>
                            <li>
                                <a className="action" key="root" onClick={this._setTag.bind(this, [])}>root</a>
                                {facetsContent}
                            </li>
                        </ul>
                        <a className="action" onClick={this._copy}>copy</a>
                    </div>

                    <div className="main-separator">Details</div>
                    <table className="main-body">
                        <tbody>
                            {detailsContent}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

ReportDetails.propTypes = {
    params: React.PropTypes.object.isRequired
};

ReportDetails.contextTypes = {
    router: React.PropTypes.object.isRequired
};

export default ReportDetails;
