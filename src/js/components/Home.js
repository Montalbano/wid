import React from "react";

class Home extends React.Component {

    render() {
        return (
            <div className="home">
                <div className="site-summary">
                    <p>&ldquo; Wid is your best companion for tracking your time. You can follow your personal and professional time.</p>
                    <p>The innovation holds in the tag system, you organize your day with tags and get your own reports. &rdquo;</p>
                </div>

                <div className="site-presentation">
                    <div className="site-img timeline"></div>
                    <div className="site-legend">
                        <span className="">Timeline</span>
                        <p>The timeline is your dashboard to handle your day.</p></div>
                </div>

                <div className="site-presentation">
                    <div className="site-legend">
                        <span className="">Report</span>
                        <p>Create your own reports and follow the progression.</p></div>
                    <div className="site-img report"></div>
                </div>

                <div className="site-presentation last">
                    <div className="site-img application"></div>
                    <div className="site-legend">
                        <span className="">Application</span>
                        <p>Plug applications with Wid and share data from github, gitlab, <a href={window.serverContext + "/wid-ext.crx"}>chrome extension</a>, ...</p>
                    </div>
                </div>
            </div>
        );
    }
}

export default Home;
