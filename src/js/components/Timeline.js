import React from "react";
import moment from "moment";

import TimelineStore from "../stores/TimelineStore";
import {diffTime} from "../utils/DateTimeUtils";
import Item from "./Item";
import Filter from "./Filter";
import ReportWidgets from "./ReportWidgets";
import TimelineInput from "./TimelineInput";

function getStateFromStores() {
    return {
        activities: TimelineStore.getActivities(),
        beginDateTime: TimelineStore.getBeginDateTime(),
        filter: TimelineStore.getFilter()
    };
}

class Timeline extends React.Component {

    constructor() {
        super();
        this.state = getStateFromStores();
        this._onChange = this._onChange.bind(this);
    }

    componentDidMount() {
        TimelineStore.addChangeListener(this._onChange);
    }

    componentWillUnmount() {
        TimelineStore.removeChangeListener(this._onChange);
    }

    _onChange() {
        if (!TimelineStore.isPending()) {
            this.setState(getStateFromStores());
        }
    }

    render() {
        let previousDate = moment(this.state.beginDateTime).format("L");

        let items = [];
        this.state.activities.forEach((activity, index) => {

            let beginDateTime = moment(activity.beginDateTime);
            let endDateTime = moment(activity.endDateTime);

            let nextDate = beginDateTime.format("ll");
            if (nextDate !== previousDate) {
                previousDate = nextDate;
                items.push(<div className="date-separator" key={"date_" + index}>{nextDate}</div>);
            }

            let time = beginDateTime.format("LTS") + " - " + endDateTime.format("LTS");
            let duration = diffTime(beginDateTime, endDateTime);

            let subClass = !activity.proposition ? "" : "proposition";

            items.push(<Item activity={activity}
                            datetime={time}
                            duration={duration}
                            key={"item_" + index}
                            subClass={subClass}/>);
        });

        return (
            <div className="timeline">
                {this.props.children}
                <div className="items">
                    <TimelineInput />
                    <div className="item--charts">
                        <ReportWidgets period={this.state.filter}/>
                        <Filter />
                    </div>
                    {items}
                </div>
            </div>
        );
    }
}

Timeline.propTypes = {
    children: React.PropTypes.object
};

export default Timeline;
