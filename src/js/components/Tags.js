/*eslint no-alert: 0*/
import React from "react";
import TagStore from "../stores/TagStore";
import {search, update} from "../actions/TagActions";

function getStateFromStores() {
    return {
        rows: TagStore.getRows(),
        removed: new Set(),
        added: new Set()
    };
}

class Tags extends React.Component {

    constructor() {
        super();

        this.state = getStateFromStores();
        this._onChange = this._onChange.bind(this);
        this._search = this._search.bind(this);
        this._update = this._update.bind(this);
        this._displayTagRemoved = this._displayTagRemoved.bind(this);
        this._displayTagAdded = this._displayTagAdded.bind(this);
    }

    componentDidMount() {
        TagStore.addChangeListener(this._onChange);
    }

    componentWillUnmount() {
        TagStore.removeChangeListener(this._onChange);
    }

    _onChange() {
        this.setState(Object.assign(getStateFromStores(), {
            added: this.state.added,
            removed: this.state.removed
        }));
    }

    _search(event) {
        event.preventDefault();

        let {query, beginDate, endDate} = this.refs;
        search(query.value, beginDate.value, endDate.value);
    }

    _update(event) {
        event.preventDefault();

        let confirmed = confirm("Are you sure to do replace all ?");
        if (confirmed) {
            let {query, beginDate, endDate, before, after} = this.refs;
            update(query.value, beginDate.value, endDate.value, Array.from(this.state.removed), Array.from(this.state.added));

            this.setState(Object.assign(this.state, {
                removed: new Set(),
                added: new Set()
            }));

            before.value = "";
            after.value = "";
        }
    }

    _displayTagRemoved() {
        let {before} = this.refs;
        let values = before.value.trim().split(" ");

        this.setState(Object.assign(this.state, {
            removed: new Set(values)
        }));
    }

    _displayTagAdded() {
        let {after} = this.refs;
        let values = after.value.trim().split(" ");

        this.setState(Object.assign(this.state, {
            added: new Set(values)
        }));
    }

    renderFacets(rows, tagsList = [], totalCount = []) {
        let keys = Object.keys(rows);

        let items = keys.map((key) => {
            if (key !== "__meta__") {
                let {tag, count} = rows[key].__meta__;

                if (tagsList.length) {
                    let v = totalCount.pop();
                    v += count;
                    totalCount.push(v);
                }

                let tags = tagsList.concat(tag);
                let values = totalCount.concat(0);
                let child = this.renderFacets(rows[key], tags, values);

                let childTotalCount = values[values.length - 1];
                if (childTotalCount !== count && childTotalCount <= count) {
                    let tagValues = tags.concat(Array.from(this.state.added))
                    .map((tagValue, index) => {
                        let classes = "tag";
                        if (this.state.added.has(tagValue)) {
                            classes += " added";
                        }
                        if (this.state.removed.has(tagValue)) {
                            classes += " removed";
                        }

                        return (
                            <span className={classes} key={index}>{tagValue}</span>
                        );
                    });

                    child.push((
                        <tr className="main-item">
                            <td>{tagValues}</td>
                            <td>{count}</td>
                        </tr>
                    ));
                }
                return child;
            }
            return [];
        });

        return items;
    }

    render() {
        let rows = this.renderFacets(this.state.rows);

        return (
            <div className="tags-main main">
                <div className="main-table">
                    <div className="main-separator">Search</div>
                    <div className="main-form">
                        <form onSubmit={this._search}>
                            <div>
                                <label>Query:</label>
                                <input ref="query" type="text"/>
                            </div>
                            <div>
                                <label>Begin date:</label>
                                <input ref="beginDate" type="datetime-local"/>
                            </div>
                            <div>
                                <label>End date:</label>
                                <input ref="endDate" type="datetime-local"/>
                            </div>
                            <input className="action" type="submit" value="Show result"/>

                            <div>
                                <label>Replace:</label>
                                <input onChange={this._displayTagRemoved} ref="before" type="text"/>
                                <div className="fa fa-arrow-down"></div>
                                <input onChange={this._displayTagAdded} ref="after" type="text"/>
                            </div>
                            <input className="action" onClick={this._update} type="button" value="Replace all"/>

                        </form>
                    </div>

                    <div className="main-separator">Result</div>
                    <table className="main-body">
                        <tbody>
                            {rows}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

Tags.propTypes = {
};

Tags.contextTypes = {
};

export default Tags;
