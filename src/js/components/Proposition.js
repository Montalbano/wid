import React from "react";
import FacetStore from "../stores/FacetStore";
import {completion} from "../actions/TimelineActions";

function getStateFromStores() {
    return {
        facets: FacetStore.getValues(),
        replace: FacetStore.isReplace()
    };
}

class Proposition extends React.Component {

    constructor() {
        super();
        this.state = getStateFromStores();
        this._onChange = this._onChange.bind(this);
    }

    componentDidMount() {
        FacetStore.addChangeListener(this._onChange);
    }

    componentWillReceiveProps(props) {
        if (props.tags !== this.tags) {
            completion(props.tags);
            this.tags = props.tags;
        }
    }

    componentWillUnmount() {
        FacetStore.removeChangeListener(this._onChange);
    }

    _onChange() {
        this.setState(getStateFromStores());
    }

    _pushTag(tag) {
        this.props.onPushTag(tag, this.state.replace);
    }

    pushTagTab() {
        let firstProposition = this.refs.proposition0;
        if (firstProposition) {
            this.props.onPushTag(firstProposition.textContent, this.state.replace);
        }
    }

    render() {
        let propositions = this.state.facets.map((value, index) => {
            return (
                <div className="action" key={"proposition_" + index} onClick={this._pushTag.bind(this, value.tag)} ref={"proposition" + index}>
                    {value.tag}
                </div>
            );
        });

        return (
            <div className="propositions" tabIndex="1">
                {propositions}
            </div>
        );
    }
}

Proposition.propTypes = {
    onPushTag: React.PropTypes.func,
    tags: React.PropTypes.array
};

export default Proposition;
