import React from "react";
import ReactDOM from "react-dom";
import {Link} from "react-router";
import {addActivity, saveActivity} from "../actions/TimelineActions";
import {hashCode} from "../utils/StringUtils";

import Events from "./Events";
import Proposition from "./Proposition";

let ENTER_KEY_CODE = 13;
let TAB_KEY_CODE = 9;

function tagsToArray(tagsAsString, removeNotes) {
    let string = tagsAsString;
    if (removeNotes) {
        string = string.replace(/\(.*?(\)|$)/g, " ");
    }
    string = string.trim().replace(/\s+/g, " ");

    if (string) {
        return string.split(" ");
    }
    return [];
}

function tagsToString(tagsAsArray) {
    return tagsAsArray.join(" ");
}

function addTag(tagsAsString, tag, deleteCount = 0) {
    // Remove all space separator
    let string = tagsAsString.replace(/\s+/g, " ");

    // Add tag
    let tags = tagsToArray(string);
    if (deleteCount > 0) {
        tags.splice(-deleteCount, deleteCount, tag);
    } else {
        tags.push(tag);
    }

    // Add space after last tag
    tags.push("");

    // Return the string from the array
    return tagsToString(tags);
}

function extractNote(input) {
    let match = input.match(/\(.*?(\)|$)/g);
    if (match) {
        return match
                .join(" ")
                .replace(/\s+/g, " ")
                .replace(/\)\(/g, " ")
                .replace(/\)|\(/g, "");
    }
    return match;
}

class Item extends React.Component {

    constructor() {
        super();

        this.state = {
            edit: false,
            split: false
        };

        this._onSplit = this._onSplit.bind(this);
        this._onEdit = this._onEdit.bind(this);
        this._onBlur = this._onBlur.bind(this);
        this._onKeyDown = this._onKeyDown.bind(this);
        this._doProposition = this._doProposition.bind(this);
        this._onCopyPaste = this._onCopyPaste.bind(this);
        this._onFocus = this._onFocus.bind(this);
        this._onPushTag = this._onPushTag.bind(this);
        this._toggleEvents = this._toggleEvents.bind(this);
    }

    _onEdit() {
        this.setState({
            edit: true
        }, () => {
            this._focusInput();
        });
    }

    _focusInput() {
        let input = this.refs.input;
        let length = input.value.length;

        input.focus();
        input.setSelectionRange(length, length);
    }

    _onSplit() {
        this.setState({
            split: true
        });
    }

    _onFocus() {
        this.setState({
            edit: true
        });
    }

    _toggleEvents() {
        this.setState({
            events: !this.state.events
        });
    }

    _onBlur(e) {
        if (e.relatedTarget !== ReactDOM.findDOMNode(this.refs.proposition)) {
            this.setState({
                edit: false,
                tags: null
            });
        }
    }

    _onKeyDown(event) {
        if (event.keyCode === ENTER_KEY_CODE) {
            event.preventDefault();

            if (!this.props.pending) {
                let value = event.target.value;
                if (value) {
                    let tags = tagsToArray(value, true);
                    let notes = extractNote(value);
                    event.target.value = "";

                    let activity = this.props.activity;
                    if (this.props.subClass === "first") {
                        addActivity(tags, undefined, undefined, notes);

                    } else {
                        saveActivity(tags, activity.beginDateTime, activity.endDateTime, notes);
                        this.setState({
                            edit: false
                        });
                    }
                }
            }

        } else if (event.keyCode === TAB_KEY_CODE) {
            event.preventDefault();
            this.refs.proposition.pushTagTab();
        }
    }

    _onCopyPaste() {
        setTimeout(() => {
            this._doProposition();
        }, 0);
    }

    _onPushTag(tag, replace) {
        this.refs.input.value = addTag(this.refs.input.value, tag, +replace);
        this._doProposition();
    }

    _doProposition() {
        this.setState({
            edit: true,
            tags: tagsToArray(this.refs.input.value, true)
        }, () => {
            this.refs.input.focus();
        });
    }

    _useActivity(activity) {
        addActivity(activity.tag);
    }

    renderAction(activity) {
        let editAction = <span className="action" key={"editAction"} onClick={this._onEdit}>edit</span>;
        let mergeAction = (
            <span className="action" key="mergeAction">
                <Link className="action" to={`/timeline/merge/${activity.beginDateTime}/${activity.endDateTime}`}>
                    merge
                </Link>
            </span>
        );
        let splitAction = (
            <span className="action" key="splitAction">
                <Link className="action" to={`/timeline/split/${activity.beginDateTime}/${activity.endDateTime}`}>
                    split
                </Link>
            </span>
        );
        let useAction = (
            <div className="action" key="useAction">
                <a className="action" onClick={this._useActivity.bind(this, activity)}>
                    use
                </a>
            </div>
        );
        let eventsAction = this.state.events ? (
            <div className="fa fa-angle-up action--events" key="eventsAction" onClick={this._toggleEvents}></div>
        ) : (
            <div className="fa fa-angle-down action--events" key="eventsAction" onClick={this._toggleEvents}></div>
        );

        if (this.props.subClass === "first") {
            return [];
        } else if (activity.proposition) {
            return [editAction, eventsAction];
        }
        return [editAction, mergeAction, splitAction, useAction, eventsAction];
    }

    renderTagValue(activity) {
        let tagValue;

        if (this.state.edit || this.props.subClass === "first") {
            tagValue = (<textarea defaultValue={activity.tag && activity.tag.join(" ")}
                            onChange={this._doProposition}
                            onKeyDown={this._onKeyDown}
                            onPaste={this._onCopyPaste}
                            ref="input"/>);

        } else {
            tagValue = <span>{activity.tag && activity.tag.join(" ")}</span>;
        }

        return tagValue;
    }

    render() {
        let activity = this.props.activity;

        let tagsClass = activity.proposition ? "fa-thumb-tack" : "fa-tags";

        let hash = hashCode(activity.tag ? activity.tag.join("") : "");
        let color = Math.abs(hash % 4);

        let proposition;
        if (this.state.edit) {
            proposition = <Proposition onPushTag={this._onPushTag} ref="proposition" tags={this.state.tags || activity.tag}/>;
        }

        let events;
        if (this.props.subClass === "first" || this.state.events) {
            events = (<Events beginDateTime={activity.beginDateTime}
                             endDateTime={activity.endDateTime || Number.MAX_VALUE}
                             noActions={this.props.subClass !== "first"}
                             onPushTag={this._onPushTag}/>);
        }

        return (
            <div className="item-container">
                <div className={"item item--" + this.props.subClass}>
                    <div className={"icon fa fa-clock-o fa-2x" + " tags--color_" + color}></div>
                    <div className={"datetime datetime--" + this.props.subClass}>
                        <div>{this.props.duration}</div>
                        <div>{this.props.datetime}</div>
                    </div>

                    <div className={"icon fa fa-2x " + tagsClass}></div>
                    <div className={"tags tags--" + this.props.subClass} onBlur={this._onBlur} onFocus={this._onFocus}>
                        <div className={"tag tag--" + this.props.subClass}>
                            {this.renderTagValue(activity)}
                        </div>
                        {proposition}
                    </div>

                    <div className="icon fa fa-wrench fa-2x"></div>
                    <div className={"actions actions--" + this.props.subClass}>
                        {this.renderAction(activity)}
                    </div>
                </div>
                {events}
            </div>
        );
    }
}

Item.propTypes = {
    activity: React.PropTypes.object,
    datetime: React.PropTypes.string.isRequired,
    duration: React.PropTypes.string.isRequired,
    pending: React.PropTypes.bool,
    subClass: React.PropTypes.string.isRequired
};

export default Item;
