/*eslint no-alert: 0*/
import React from "react";
import {exportDb} from "../actions/UserActions";

class Export extends React.Component {

    constructor() {
        super();
        this._export = this._export.bind(this);
    }

    _export() {
        exportDb();
    }

    render() {
        return (
            <div className="export">
                <div className="main-separator">Export data</div>
                <div className="main-form">
                    <input className="action" onClick={this._export} type="button" value="Export"/>
                </div>
            </div>
        );
    }
}

export default Export;
