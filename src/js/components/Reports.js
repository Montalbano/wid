/*eslint no-alert: 0*/
import React from "react";
import {Link} from "react-router";
import ReportStore from "../stores/ReportStore";
import {formatPeriod} from "../utils/DateTimeUtils";
import {getReports, addReport, removeReport} from "../actions/ReportActions";

function getStateFromStores() {
    return {
        reports: ReportStore.getReports()
    };
}

class Reports extends React.Component {

    constructor() {
        super();

        this.state = getStateFromStores();

        this._onChange = this._onChange.bind(this);
        this._addReport = this._addReport.bind(this);
        this._removeReport = this._removeReport.bind(this);
    }

    componentDidMount() {
        getReports();
        ReportStore.addChangeListener(this._onChange);
    }

    componentWillUnmount() {
        ReportStore.removeChangeListener(this._onChange);
    }

    _onChange() {
        this.setState(getStateFromStores());
    }

    _addReport(event) {
        event.preventDefault();

        let name = this.refs.name.value;
        let query = this.refs.query.value;
        let period = this.refs.period.value;
        let widget = this.refs.widget.checked ? 1 : 0;

        addReport(name, query, period, widget);
    }

    _removeReport(report) {
        let confirmed = confirm(`Are you sure to remove report ${report.name} ?`);
        confirmed && removeReport(report.id);
    }

    render() {
        let reports = this.state.reports.map((report) => {
            return (
                <tr className="main-item" key={report.id}>
                    <td>{report.name}</td>
                    <td>{report.tags}</td>
                    <td>{formatPeriod(report.period)}</td>
                    <td>{report.widget === 1 ? "true" : "false"}</td>
                    <td>
                        <span className="action" onClick={this._removeReport.bind(this, report)}>Delete</span>
                    </td>
                    <td>
                        <Link className="action" to={`/report/${report.id}`}>
                            See
                        </Link>
                    </td>
                </tr>
            );
        });

        return (
            <div className="reports main">
                <div className="main-table">
                    <div className="main-separator">Create report</div>
                    <div className="main-form">
                        <form onSubmit={this._addReport}>
                            <div>
                                <label>Name:</label>
                                <input ref="name" type="text"/>
                            </div>
                            <div>
                                <label>Query:</label>
                                <input ref="query" type="text"/>
                            </div>
                            <div>
                                <label>Period:</label>
                                <select ref="period">
                                    <option value="0">Today</option>
                                    <option value="1">Week</option>
                                    <option value="2">Month</option>
                                    <option value="3">Year</option>
                                </select>
                            </div>
                            <div>
                                <label>Widget:</label>
                                <input ref="widget" type="checkbox" value="true"/>
                            </div>
                            <input className="action" type="submit" value="Add new report"/>
                        </form>
                    </div>

                    <div className="main-separator">Reports</div>
                    <table className="main-body">
                        <tbody>
                            {reports}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

Reports.propTypes = {
};

Reports.contextTypes = {
};

export default Reports;
