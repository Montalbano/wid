import React from "react";
import moment from "moment";
import {diffValue, getReferenceTime} from "../utils/DateTimeUtils";

class ReportHistory extends React.Component {

    render() {
        let number;
        let unit;
        let format;
        let formatShort;
        let period = this.props.period;
        let date = moment(this.props.date);

        if (period === 0) {
            number = 1;
            unit = "days";
            date.startOf("day");
            format = "L";
            formatShort = "D";

        } else if (period === 1) {
            number = 7;
            unit = "days";
            date.startOf("week");
            format = "w";
            formatShort = "w";

        } else if (period === 2) {
            number = 1;
            unit = "months";
            date.startOf("month");
            format = "MMMM";
            formatShort = "MMM";

        } else if (period === 3) {
            number = 1;
            unit = "years";
            date.startOf("year");
            format = "YYYY";
            formatShort = "YY";
        }

        let refTime = getReferenceTime(period);

        let bars = [];
        for (let index = 0; index < 12; index++) {

            let progress = this.props.values[index] || 0;
            let height = ~~(progress / refTime * 100);

            let title = diffValue(progress) + " for " + date.format(format);
            let selectedClass = index === 0 ? " selected" : "";

            bars.push(
                <div className="report-bar-for" key={"bar_" + index}>
                    <a className="report-bar" onClick={this.props.onClick.bind(null, +date)} title={title}>
                        <div className={"report-bar-value" + selectedClass} style={{height: height + "%"}}></div>
                    </a>
                    <div className="report-bar-title">
                        {date.format(formatShort)}
                    </div>
                </div>
            );

            date.subtract(number, unit);
        }

        return (
            <div className="report-history">
                {bars}
            </div>
        );
    }
}

ReportHistory.propTypes = {
    date: React.PropTypes.number,
    onClick: React.PropTypes.func.isRequired,
    period: React.PropTypes.number.isRequired,
    values: React.PropTypes.array.isRequired
};

ReportHistory.contextTypes = {
};

export default ReportHistory;
