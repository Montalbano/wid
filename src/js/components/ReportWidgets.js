import React from "react";
import ReportStore from "../stores/ReportStore";
import {getWidgets} from "../actions/ReportActions";
import ReportCircle from "./ReportCircle";
import {getDateTimeForPeriod} from "../utils/DateTimeUtils";

function getStateFromStores() {
    return {
        widgets: ReportStore.getWidgets()
    };
}

class ReportWidgets extends React.Component {

    constructor() {
        super();
        this.state = getStateFromStores();
        this._onChange = this._onChange.bind(this);
    }

    componentDidMount() {
        this._getWidgets(this.props);

        ReportStore.addChangeListener(this._onChange);
    }

    componentWillReceiveProps(props) {
        this._getWidgets(props);
    }

    componentWillUnmount() {
        ReportStore.removeChangeListener(this._onChange);
    }

    _getWidgets({period}) {
        let {beginDateTime, endDateTime} = getDateTimeForPeriod(period);
        getWidgets(beginDateTime, endDateTime);
    }

    _onChange() {
        this.setState(getStateFromStores());
    }

    render() {
        let widgets = this.state.widgets.map((widget, index) => {
            widget.period = this.props.period;
            return <ReportCircle className={"report-circle--color" + index % 3} key={"report" + index} widget={widget}/>;
        });

        return (
            <div className="report-widgets">
                {widgets}
            </div>
        );
    }
}

ReportWidgets.propTypes = {
    period: React.PropTypes.number.isRequired
};

ReportWidgets.contextTypes = {
};

export default ReportWidgets;
