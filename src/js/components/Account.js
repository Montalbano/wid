/*eslint no-alert: 0*/
import React from "react";
import {changePasswd} from "../actions/UserActions";

class Account extends React.Component {

    constructor() {
        super();
        this._onChangePasswd = this._onChangePasswd.bind(this);
    }

    _onChangePasswd(event) {
        event.preventDefault();

        let oldPasswd = this.refs.oldPasswd.value;
        let newPasswd = this.refs.newPasswd.value;
        let confirm = this.refs.confirm.value;

        changePasswd(oldPasswd, newPasswd, confirm);

        this.refs.oldPasswd.value = "";
        this.refs.newPasswd.value = "";
        this.refs.confirm.value = "";
    }

    render() {
        return (
            <div className="account">
                <div className="main-separator">Modify password</div>
                <div className="main-form">
                    <form onSubmit={this._onChangePasswd}>
                        <div>
                            <label>Old password:</label>
                            <input ref="oldPasswd" required="true" type="password"/>
                        </div>
                        <div>
                            <label>New password:</label>
                            <input ref="newPasswd" required="true" type="password"/>
                        </div>
                        <div>
                            <label>Confirm the new password:</label>
                            <input ref="confirm" required="true" type="password"/>
                        </div>
                        <input className="action" type="submit" value="Modify password"/>
                    </form>
                </div>
            </div>
        );
    }
}

Account.propTypes = {
};

export default Account;
