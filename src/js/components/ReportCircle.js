import React from "react";
import {diffValue, getReferenceTime} from "../utils/DateTimeUtils";

class ReportCircle extends React.Component {

    render() {
        let deg50 = 0;
        let deg100 = 0;
        let hide;

        if (this.props.widget) {
            let {time, period} = this.props.widget;

            let refTime = getReferenceTime(period);

            let progress = time / refTime;
            if (progress > 1) {
                progress = 1;
            }
            if (progress <= 0.5) {
                hide = "none";
                deg50 = (180 * progress * 2) - 180;
                deg100 = 0;

            } else {
                hide = "initial";
                deg50 = 0;
                deg100 = (180 * (progress - 0.5) * 2) - 180;
            }
        }

        return (
            <div className="report-circle">
                <div className="report-circle-chart">
                    <div className={"report-circle-50 " + this.props.className} style={{transform: "rotateZ(" + deg50 + "deg)"}}>
                    </div>
                    <div className="report-circle-hide">
                    </div>
                    <div className={"report-circle-100 " + this.props.className} style={{transform: "rotateZ(" + deg100 + "deg)", display: hide}}>
                    </div>
                    <div className="report-circle-inner">
                    </div>
                </div>

                <div>
                    {this.props.widget ? this.props.widget.name : ""}
                </div>
                <div>
                    {this.props.widget ? diffValue(this.props.widget.time) : 0}
                </div>
            </div>
        );
    }
}

ReportCircle.propTypes = {
    className: React.PropTypes.string,
    widget: React.PropTypes.object.isRequired
};

ReportCircle.contextTypes = {
};

export default ReportCircle;
