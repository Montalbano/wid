/*eslint no-alert: 0*/
import React from "react";
import moment from "moment";

import {getEvents, removeEvent} from "../actions/EventActions";
import EventStore from "../stores/EventStore";

import {addActivity} from "../actions/TimelineActions";
import {diffValue} from "../utils/DateTimeUtils";

function getStateFromStores(beginDateTime, endDateTime) {
    return {
        events: EventStore.getEvents(beginDateTime, endDateTime)
    };
}

class Events extends React.Component {

    constructor({beginDateTime, endDateTime}) {
        super();

        this.state = {events: []};
        getEvents(beginDateTime, endDateTime);

        this._onChange = this._onChange.bind(this);
    }

    componentDidMount() {
        EventStore.addChangeListener(this._onChange);
    }

    componentWillUnmount() {
        EventStore.removeChangeListener(this._onChange);
    }

    _onChange() {
        this.setState(getStateFromStores(this.props.beginDateTime, this.props.endDateTime));
    }

    _pushTag(tag) {
        this.props.onPushTag(tag, false);
    }

    _removeEvent(event) {
        let tags = event.value.trim().split(" ");
        let confirmed = confirm(`Are you sure to remove event \"${tags}\" ?`);
        confirmed && removeEvent(event.id);
    }

    render() {
        let events = this.state.events && this.state.events.map((event, index) => {

            let tags = event.value.trim().split(" ");

            let actions;
            if (!this.props.noActions) {
                actions = [
                    <div className="action" key="0" onClick={addActivity.bind(null, tags, event.beginDateTime, event.endDateTime, null)}>Use</div>,
                    <div className="action" key="1" onClick={this._pushTag.bind(this, tags)}>Add tag</div>
                ];
            } else {
                actions = [
                    <div className="action" key="3" onClick={this._removeEvent.bind(this, event)}>Remove</div>
                ];
            }

            return (
                <div className="event" key={"event_" + index}>
                    <div>{event.source}</div>
                    <div>{event.value}</div>
                    <div>{diffValue(event.endDateTime - event.beginDateTime)}</div>
                    <div>{moment(event.beginDateTime).format("LTS")} - {moment(event.endDateTime).format("LTS")}</div>
                    {actions}
                </div>
            );
        });

        return (
            <div className="events">
                {events}
            </div>
        );
    }
}

Events.propTypes = {
    beginDateTime: React.PropTypes.number,
    endDateTime: React.PropTypes.number,
    noActions: React.PropTypes.bool,
    onPushTag: React.PropTypes.func
};

export default Events;
