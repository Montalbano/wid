import React from "react";
import {resetPasswd} from "../actions/UserActions";

class ResetPassword extends React.Component {

    constructor() {
        super();
        this._onResetPasswd = this._onResetPasswd.bind(this);
    }

    _onResetPasswd(event) {
        event.preventDefault();
        resetPasswd(this.refs.email.value);
    }

    render() {
        return (
            <div className="signin">
                <div className="logo">
                    <div className="fa-stack fa-lg">
                      <i className="fa fa-circle-o fa-stack-2x"></i>
                      <i className="fa fa-tag fa-stack-1x"></i>
                    </div>
                </div>
                <div className="signin-title">Reset your password</div>
                <div className="signin-box">
                    <form onSubmit={this._onResetPasswd}>
                        <p>Email</p>
                        <p><input placeholder="Your email" ref="email" required="true" type="email"/></p>
                        <input className="action" type="submit" value="Send password"/>
                    </form>
                </div>
            </div>
        );
    }
}


ResetPassword.propTypes = {
};

ResetPassword.contextTypes = {
};

export default ResetPassword;
