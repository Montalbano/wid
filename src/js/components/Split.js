import React from "react";
import moment from "moment";

import {diffTime, diffValue} from "../utils/DateTimeUtils";
import {modifyActivity} from "../actions/TimelineActions";

class Split extends React.Component {

    constructor({params}) {
        super();

        this.state = {
            beginDate: +params.begin,
            endDate: +params.end
        };

        this._onRangeChange = this._onRangeChange.bind(this);
        this._onCancel = this._onCancel.bind(this);
        this._onSave = this._onSave.bind(this);
    }

    _onRangeChange(event) {
        let beginInput = this.refs.beginInput;
        let endInput = this.refs.endInput;

        let beginValue = +beginInput.value;
        let endValue = +endInput.value;

        if (beginValue > endValue) {
            if (event.target === beginInput) {
                endValue = beginValue;
                endInput.value = beginInput.value;
            } else {
                beginValue = endValue;
                beginInput.value = endInput.value;
            }
        }

        this.setState({
            beginDate: beginValue,
            endDate: endValue
        });
    }

    _onCancel() {
        this.context.router.replace("/timeline");
    }

    _onSave() {
        modifyActivity(+this.props.params.begin, +this.props.params.end, this.state.beginDate, this.state.endDate);
        this.context.router.replace("/timeline");
    }

    render() {
        let duration = diffTime(moment(this.state.beginDate), moment(this.state.endDate));

        let beginDate = this.props.params.begin;
        let endDate = this.props.params.end;

        let diff = diffValue((this.state.beginDate - this.props.params.begin) + (this.props.params.end - this.state.endDate));

        return (
            <div className="popup">
                <div>
                    <label>Begin date:</label> {moment(this.state.beginDate).format("LTS")}
                    <input defaultValue={beginDate} max={endDate} min={beginDate} onInput={this._onRangeChange} ref="beginInput" type="range" />
                </div>
                <div>
                    <label>End date:</label> {moment(this.state.endDate).format("LTS")}
                    <input defaultValue={endDate} max={endDate} min={beginDate} onInput={this._onRangeChange} ref="endInput" type="range" />
                </div>
                <div>{"Duration: " + duration + " [-" + diff + "]"}</div>
                <div className="actions">
                    <div className="action fa fa-remove" onClick={this._onCancel}>Cancel</div>
                    <div className="action fa fa-check" onClick={this._onSave}>Save</div>
                </div>
            </div>
        );
    }
}


Split.propTypes = {
    params: React.PropTypes.object
};

Split.contextTypes = {
    router: React.PropTypes.object.isRequired
};

export default Split;
