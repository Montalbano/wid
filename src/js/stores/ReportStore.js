import AppDispatcher from "../dispatcher/AppDispatcher";
import {EventEmitter} from "events";

let CHANGE_EVENT = "change";

class ReportStore extends EventEmitter {

    constructor() {
        super();
        this.init();
    }

    init() {
        this._reports = new Map();
        this._details = new Map();
        this._widgets = [];
    }

    getReports() {
        return Array.from(this._reports.values());
    }

    getReport(id) {
        return this._reports.get(id);
    }

    setReports(reports) {
        reports.forEach((report) => {
            this.addReport(report);
        });
    }

    addReport({id, name, tags, period, widget}) {
        this._reports.set(id, {id, name, tags, period, widget});
    }

    removeReport(id) {
        this._reports.delete(id);
        this._details.delete(id);
    }

    setDetails({id, date, total, details, history, facets, notes}) {
        this._details.set(id, {id, date, total, details, history, facets, notes});
    }

    getDetails(id, date) {
        let detail = this._details.get(id);
        if (detail && detail.date === date) {
            return detail;
        }

        return {
            details: [],
            history: [],
            facets: [],
            notes: []
        };
    }

    getWidgets() {
        return this._widgets;
    }

    setWidgets(widgets) {
        this._widgets = widgets;
    }

    emitChange() {
        this.emit(CHANGE_EVENT);
    }

    addChangeListener(callback) {
        this.on(CHANGE_EVENT, callback);
    }

    removeChangeListener(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
}

let reportStore = new ReportStore();

AppDispatcher.register(function(action) {
    switch (action.type) {

    case "GET_REPORTS":
        reportStore.setReports(action.rows);
        reportStore.emitChange();
        break;

    case "ADD_REPORT":
        reportStore.addReport(action);
        reportStore.emitChange();
        break;

    case "REMOVE_REPORT":
        reportStore.removeReport(action.id);
        reportStore.emitChange();
        break;

    case "GET_WIDGETS":
        reportStore.setWidgets(action.widgets);
        reportStore.emitChange();
        break;

    case "GET_DETAILS":
        reportStore.setDetails(action);
        reportStore.emitChange();
        break;

    case "SIGNOUT_DONE":
    case "USER_LOGOUT":
        reportStore.init();
        reportStore.emitChange();
        break;

    default:
        // Nothing to do
    }
});

export default reportStore;
