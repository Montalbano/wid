import AppDispatcher from "../dispatcher/AppDispatcher";
import {EventEmitter} from "events";

let CHANGE_EVENT = "change";

class EventStore extends EventEmitter {

    constructor() {
        super();
        this._data = [];
    }

    getEvents(beginDateTime, endDateTime) {
        return this._data.filter((event) => {
            return event.beginDateTime >= beginDateTime && event.beginDateTime < endDateTime;
        });
    }

    setEvents(events, beginDateTime, endDateTime) {
        this._data = this._data.filter((event) => {
            return event.beginDateTime < beginDateTime || event.beginDateTime >= endDateTime;
        });
        this._data = this._data.concat(events);
    }

    updateEvents(events) {
        events.forEach((newEvent) => {
            let {id, beginDateTime, endDateTime, source, value} = newEvent;

            if (id) {
                this._data.unshift({id, beginDateTime, endDateTime, source, value});
            } else {
                this._data.forEach((event) => {
                    if (event.source === source && event.beginDateTime === event.endDateTime) {
                        event.endDateTime = endDateTime;
                    }
                });
            }
        });
    }

    removeEvent(id) {
        this._data = this._data.filter((event) => {
            return event.id !== id;
        });
    }

    emitChange() {
        this.emit(CHANGE_EVENT);
    }

    addChangeListener(callback) {
        this.on(CHANGE_EVENT, callback);
    }

    removeChangeListener(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
}

let eventStore = new EventStore();

AppDispatcher.register(function(action) {
    switch (action.type) {

    case "GET_EVENTS":
        eventStore.setEvents(action.rows, action.beginDateTime, action.endDateTime);
        eventStore.emitChange();
        break;

    case "UPDATE_EVENTS":
        eventStore.updateEvents(action.events);
        eventStore.emitChange();
        break;

    case "ADD_ACTIVITY":
        eventStore.emitChange();
        break;

    case "REMOVE_EVENT":
        eventStore.removeEvent(action.id);
        eventStore.emitChange();
        break;

    default:
        // Nothing to do
    }
});

export default eventStore;
