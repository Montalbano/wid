import AppDispatcher from "../dispatcher/AppDispatcher";
import {EventEmitter} from "events";

let CHANGE_EVENT = "change";

class TimelineStore extends EventEmitter {

    constructor() {
        super();
        this._activities = [];
        this._filter = 0;
        this._pending = false;
    }

    getActivities() {
        let current = {
            beginDateTime: this._beginDateTime
        };
        let result = [];

        this._activities.forEach((value) => {
            // Detect groupping tags
            if (current.beginDateTime === value.beginDateTime &&
                current.endDateTime === value.endDateTime) {
                current.tag.push(value.tag);

            } else {
                // Hole in the timeline
                if (value.endDateTime !== current.beginDateTime
                        && value.endDateTime < current.beginDateTime
                        && value.beginDateTime !== current.beginDateTime) {

                    result.push({
                        beginDateTime: value.endDateTime,
                        endDateTime: current.beginDateTime,
                        tag: ["Please-complete-the-partial-timeline"],
                        proposition: true
                    });
                }

                // Otherwise create
                current = {
                    beginDateTime: value.beginDateTime,
                    endDateTime: value.endDateTime,
                    tag: [value.tag]
                };
                result.push(current);
            }
        });
        return result;
    }

    setBeginDateTime(beginDateTime) {
        this._beginDateTime = beginDateTime;
    }

    getBeginDateTime() {
        return this._beginDateTime;
    }

    setPending() {
        this._pending = true;
    }

    isPending() {
        return this._pending;
    }

    _createActivityItem(value, beginDateTime, endDateTime) {
        return {
            beginDateTime,
            endDateTime,
            tag: value
        };
    }

    addActivity({tag, beginDateTime, endDateTime}) {
        beginDateTime = beginDateTime || this._beginDateTime;
        this._pending = false;
        this._tag = [];

        let oldTags = [];
        let intersection = this._activities.filter((value) => {
            if (beginDateTime === value.endDateTime) {
                oldTags.push(value.tag);
                return tag.indexOf(value.tag) !== -1;
            }
            return false;
        });

        if (intersection.length === tag.length && intersection.length === oldTags.length) {
            this._activities.forEach((value) => {
                if (beginDateTime === value.endDateTime) {
                    value.endDateTime = endDateTime;
                }
            });

        } else {
            tag.forEach((value) => {
                let activity = this._createActivityItem(value, beginDateTime || this._beginDateTime, endDateTime);
                this._activities.unshift(activity);
            });
        }

        this._beginDateTime = endDateTime;
    }

    removeActivity({beginDateTime, endDateTime}) {
        this._activities = this._activities.filter((value) => {
            return beginDateTime !== value.beginDateTime ||
                endDateTime !== value.endDateTime;
        });
    }

    updateActivity({tag, beginDateTime, endDateTime}) {
        this._pending = false;

        let newActivities = tag.map((value) => {
            return this._createActivityItem(value, beginDateTime, endDateTime);
        });

        let indexBegin = null;
        let deleteCount = 0;
        this._activities.forEach((value, index) => {
            if (beginDateTime === value.beginDateTime &&
                endDateTime === value.endDateTime) {
                deleteCount++;

                if (indexBegin === null) {
                    // First time get the first index of the tags
                    indexBegin = index;
                }

            } else if (endDateTime === value.beginDateTime) {
                // Hole case
                indexBegin = index + 1;
            }
        });

        this._activities.splice(indexBegin, deleteCount, ...newActivities);
    }

    modifyActivityDates({oldBeginDateTime, oldEndDateTime, newBeginDateTime, newEndDateTime}) {
        this._pending = false;

        if (this._beginDateTime === oldEndDateTime) {
            this._beginDateTime = newEndDateTime;
        }

        this._activities.forEach((value) => {
            if (oldBeginDateTime === value.beginDateTime &&
                oldEndDateTime === value.endDateTime) {

                value.beginDateTime = newBeginDateTime;
                value.endDateTime = newEndDateTime;
            }
        });

        // Search duplicate tag
        let tags = new Set();
        this._activities = this._activities.filter((value) => {
            if (newBeginDateTime === value.beginDateTime &&
                newEndDateTime === value.endDateTime) {

                if (tags.has(value.tag)) {
                    return false;
                }
                tags.add(value.tag);
            }
            return true;
        });
    }

    getActiviesCurrentNextPrevious({oldBeginDateTime, oldEndDateTime}) {
        this._pending = false;

        let previous;
        let current;
        let next;

        this._activities.forEach((value) => {

            if (oldBeginDateTime === value.beginDateTime &&
                oldEndDateTime === value.endDateTime) {
                current = value;

            } else if (current && !previous) {
                previous = value;

            } else if (!current) {
                next = value;
            }

        });

        return {previous, current, next};
    }

    addActivities(activities) {
        this._activities = activities;
    }

    getFilter() {
        return this._filter;
    }

    setFilter(value) {
        this._filter = value;
    }

    emitChange() {
        this.emit(CHANGE_EVENT);
    }

    addChangeListener(callback) {
        this.on(CHANGE_EVENT, callback);
    }

    removeChangeListener(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
}

let timelineStore = new TimelineStore();

AppDispatcher.register(function(action) {
    let {oldBeginDateTime, oldEndDateTime, overwrite} = action;

    switch (action.type) {

    case "USER_LOGIN":
    case "SIGNIN_DONE":
        timelineStore.setBeginDateTime(action.beginDateTime || Date.now());
        timelineStore.emitChange();
        break;

    case "PENDING":
        timelineStore.setPending();
        timelineStore.emitChange();
        break;

    case "ADD_ACTIVITY":
        timelineStore.addActivity(action);
        timelineStore.emitChange();
        break;

    case "UPDATE_ACTIVITY":
        timelineStore.updateActivity(action);
        timelineStore.emitChange();
        break;

    case "MODIFY_ACTIVITY":
        timelineStore.modifyActivityDates(action);
        timelineStore.emitChange();
        break;

    case "MERGE_UP_ACTIVITY":
        let {next} = timelineStore.getActiviesCurrentNextPrevious(action);

        let newEndDateTime;
        if (oldEndDateTime === next.beginDateTime) {
            newEndDateTime = next.endDateTime;

            if (overwrite) {
                timelineStore.removeActivity({beginDateTime: next.beginDateTime, endDateTime: next.endDateTime});

            } else {
                timelineStore.modifyActivityDates({
                    oldBeginDateTime: next.beginDateTime, oldEndDateTime: next.endDateTime,
                    newBeginDateTime: oldBeginDateTime, newEndDateTime: next.endDateTime
                });
            }
        } else {
            newEndDateTime = next.beginDateTime;
        }

        timelineStore.modifyActivityDates({
            oldBeginDateTime, oldEndDateTime,
            newBeginDateTime: oldBeginDateTime, newEndDateTime
        });

        timelineStore.emitChange();
        break;

    case "MERGE_DOWN_ACTIVITY":
        let {previous} = timelineStore.getActiviesCurrentNextPrevious(action);

        let newBeginDateTime;
        if (oldBeginDateTime === previous.endDateTime) {
            newBeginDateTime = previous.beginDateTime;

            if (overwrite) {
                timelineStore.removeActivity({beginDateTime: previous.beginDateTime, endDateTime: previous.endDateTime});

            } else {
                timelineStore.modifyActivityDates({
                    oldBeginDateTime: previous.beginDateTime, oldEndDateTime: previous.endDateTime,
                    newBeginDateTime: previous.beginDateTime, newEndDateTime: oldEndDateTime
                });
            }
        } else {
            newBeginDateTime = previous.endDateTime;
        }

        timelineStore.modifyActivityDates({
            oldBeginDateTime, oldEndDateTime,
            newBeginDateTime, newEndDateTime: oldEndDateTime
        });

        timelineStore.emitChange();
        break;

    case "RECEIVE_ACTIVITIES":
        timelineStore.addActivities(action.rows);
        timelineStore.emitChange();
        break;

    case "FILTER_CHANGE":
        timelineStore.setFilter(action.value);
        timelineStore.addActivities(action.rows);
        timelineStore.emitChange();
        break;

    default:
        // Nothing to do
    }
});

export default timelineStore;
