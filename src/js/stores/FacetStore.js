import AppDispatcher from "../dispatcher/AppDispatcher";
import {EventEmitter} from "events";

let CHANGE_EVENT = "change";

class FacetStore extends EventEmitter {

    constructor() {
        super();
        this.init();
    }

    init() {
        this._values = [];
    }

    getValues() {
        return this._values;
    }

    getTag() {
        return this._tag;
    }

    set(tag, values, replace) {
        this._tag = tag;
        this._replace = replace;
        this._values = values;
    }

    isReplace() {
        return this._replace === true;
    }

    emitChange() {
        this.emit(CHANGE_EVENT);
    }

    addChangeListener(callback) {
        this.on(CHANGE_EVENT, callback);
    }

    removeChangeListener(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
}

let facetStore = new FacetStore();

AppDispatcher.register(function(action) {
    switch (action.type) {

    case "GET_FACETS":
        facetStore.set(action.tag, action.rows, action.replace);
        facetStore.emitChange();
        break;

    case "SIGNOUT_DONE":
    case "USER_LOGOUT":
        facetStore.init();
        facetStore.emitChange();
        break;

    default:
        // Nothing to do
    }
});

export default facetStore;
