import AppDispatcher from "../dispatcher/AppDispatcher";
import {EventEmitter} from "events";

let CHANGE_EVENT = "change";

class TagStore extends EventEmitter {

    constructor() {
        super();
        this.init();
    }

    init() {
        this._rows = {};
    }

    getRows() {
        return this._rows;
    }

    setRows(rows) {
        this._rows = rows;
    }

    emitChange() {
        this.emit(CHANGE_EVENT);
    }

    addChangeListener(callback) {
        this.on(CHANGE_EVENT, callback);
    }

    removeChangeListener(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
}

let tagStore = new TagStore();

AppDispatcher.register(function(action) {
    switch (action.type) {

    case "GET_ROWS":
        tagStore.setRows(action.rows);
        tagStore.emitChange();
        break;

    case "SIGNOUT_DONE":
    case "USER_LOGOUT":
        tagStore.init();
        tagStore.emitChange();
        break;

    default:
        // Nothing to do
    }
});

export default tagStore;
