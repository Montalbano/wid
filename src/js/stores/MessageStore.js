import AppDispatcher from "../dispatcher/AppDispatcher";
import {EventEmitter} from "events";

let CHANGE_EVENT = "change";

class MessageStore extends EventEmitter {

    get TYPE_MESSAGE() {
        return "message";
    }
    get TYPE_ERROR() {
        return "error";
    }
    get TYPE_INFO() {
        return "info";
    }

    constructor() {
        super();
        this._message = "";
        this._type = this.TYPE_INFO;
    }

    setError(error) {
        this._message = error;
        this._type = this.TYPE_MESSAGE;
    }

    setMessage(message) {
        this._message = message;
        this._type = this.TYPE_ERROR;
    }

    setInfo(message) {
        this._message = message;
        this._type = this.TYPE_INFO;
    }

    getMessage() {
        return this._message;
    }

    getType() {
        return this._type;
    }

    emitChange() {
        this.emit(CHANGE_EVENT);
    }

    addChangeListener(callback) {
        this.on(CHANGE_EVENT, callback);
    }

    removeChangeListener(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    }
}

let messageStore = new MessageStore();

AppDispatcher.register(function(action) {
    switch (action.type) {

    case "SIGNIN_ERROR":
        messageStore.setError("Invalid username or password.");
        messageStore.emitChange();
        break;

    case "CHANGE_PASSWD_ERROR_CONFIRM":
    case "SIGNUP_ERROR_CONFIRM":
        messageStore.setError("The password and the confirmation is not the same.");
        messageStore.emitChange();
        break;

    case "RESET_PASSWD_DONE":
        messageStore.setMessage("The email with new password is sent.");
        messageStore.emitChange();
        break;

    case "RESET_PASSWD_ERROR":
        messageStore.setError("Invalid email.");
        messageStore.emitChange();
        break;

    case "CHANGE_PASSWD_DONE":
        messageStore.setMessage("The password is modified.");
        messageStore.emitChange();
        break;

    case "CHANGE_PASSWD_ERROR":
        messageStore.setError("Old password invalid.");
        messageStore.emitChange();
        break;

    case "SYNC_OFFLINE":
        messageStore.setInfo("Syncronization pending.");
        messageStore.emitChange();
        break;

    case "ONLINE":
        messageStore.setInfo("You are online.");
        messageStore.emitChange();
        break;

    case "OFFLINE":
        messageStore.setInfo("You are offline !");
        messageStore.emitChange();
        break;

    case "CLEAR_MESSAGE":
        messageStore.setMessage();
        messageStore.emitChange();
        break;

    case "SIGNUP_ERROR":
        messageStore.setError("Username already used.");
        messageStore.emitChange();
        break;

    default:
        messageStore.setMessage();
        messageStore.emitChange();
        break;
    }
});

export default messageStore;
