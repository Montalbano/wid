import io from "socket.io-client";
import {eventUpdate} from "../actions/EventActions";

let socket;

export let connect = () => {
    socket = io.connect({path: window.serverContext + "/ws"});
    socket.on("eventUpdate", eventUpdate);
};

export let disconnect = () => {
    socket.disconnect();
};
