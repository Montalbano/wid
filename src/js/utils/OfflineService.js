import {setOnline, setOffline, syncOffline} from "../actions/OnlineActions";
import TimelineStore from "../stores/TimelineStore";
import {addActivity} from "../actions/TimelineActions";

let OFFLINE_ACTIVITIES = "OFFLINE_ACTIVITIES";
let OFFLINE_BEGINDATE = "OFFLINE_BEGINDATE";

export let getOfflineActivities = () => {
    let activities = localStorage.getItem(OFFLINE_ACTIVITIES);
    return activities ? JSON.parse(activities) : [];
};

export let getOfflineBeginDate = () => {
    let beginDateTime = localStorage.getItem(OFFLINE_BEGINDATE);
    return beginDateTime ? JSON.parse(beginDateTime) : null;
};

export let clearOfflineData = () => {
    localStorage.removeItem(OFFLINE_ACTIVITIES);
    localStorage.removeItem(OFFLINE_BEGINDATE);
};

export let syncOfflineData = () => {
    let promise = Promise.resolve();
    let activities = getOfflineActivities();
    if (activities && activities.length) {
        syncOffline();

        let promises = activities.map((item) => {
            return addActivity([item.tag], item.beginDateTime, item.endDateTime);
        });

        promise = Promise.all(promises).then(clearOfflineData);
    }
    return promise;
};

// Fake the fetch call
export let fetchOffline = (url) => {
    return Promise.resolve({
        status: 0,
        json: () => {
            if (url.startsWith("/api/user")) {
                return Promise.resolve({
                    beginDateTime: getOfflineBeginDate()
                });

            } else if (url.startsWith("/api/times")) {
                return Promise.resolve({
                    rows: getOfflineActivities()
                });
            }

            // Default case
            return Promise.resolve({
                rows: [],
                widgets: []
            });
        }
    });
};

let storeActivities = () => {
    if (TimelineStore._activities.length) {
        localStorage.setItem(OFFLINE_ACTIVITIES, JSON.stringify(TimelineStore._activities));
        localStorage.setItem(OFFLINE_BEGINDATE, JSON.stringify(TimelineStore._beginDateTime));
    }
};

let changeStatus = () => {
    if (window.online) {
        TimelineStore.removeChangeListener(storeActivities);
        setOnline();
    } else {
        TimelineStore.addChangeListener(storeActivities);
        setOffline();
    }
};
// changeStatus();

let pollOnlineFile = () => {
    fetch(window.serverContext + "/assets/online.js")
    .then((response) => {
        return response.text();
    })
    .then((value) => {
        let online = value.startsWith("window.online = true;");

        if (window.online !== online) {
            window.online = online;
            changeStatus();
        }
    });

    setTimeout(pollOnlineFile, 10 * 1000);
};
pollOnlineFile();
