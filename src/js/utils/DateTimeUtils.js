import moment from "moment";

export let diffValue = function(diff) {
    let hours = ~~(diff / (1000 * 60 * 60));
    diff -= hours * 1000 * 60 * 60;

    let minutes = ~~(diff / (1000 * 60));
    diff -= minutes * 1000 * 60;

    let seconds = ~~(diff / 1000);
    diff -= seconds * 1000;

    return (hours < 10 ? "0" + hours : hours) + ":" +
            (minutes < 10 ? "0" + minutes : minutes) + ":" +
            (seconds < 10 ? "0" + seconds : seconds);
};

export let diffTime = function(beginDate, endDate) {
    let diff = endDate.diff(beginDate);
    return diffValue(diff);
};

export let getReferenceTime = function(period) {
    let refTime = 0;
    if (period === 0) { // day
        refTime = 10 * 60 * 60 * 1000;
    } if (period === 1) { // week
        refTime = 5 * 10 * 60 * 60 * 1000;
    } if (period === 2) { // month
        refTime = 20 * 5 * 10 * 60 * 60 * 1000;
    } if (period === 3) { // year
        refTime = 12 * 20 * 5 * 10 * 60 * 60 * 1000;
    }
    return refTime;
};

export let formatPeriod = function(period) {
    return [
        "TODAY",
        "WEEK",
        "MONTH",
        "YEAR"
    ][period];
};

export let getDateTimeForPeriod = function(period, refDateTime) {
    let beginDateTime = refDateTime ? moment(+refDateTime) : moment();
    let endDateTime;

    if (period === 0) {
        beginDateTime.startOf("day");

        endDateTime = moment(beginDateTime);
        endDateTime.endOf("day");

    } else if (period === 1) {
        beginDateTime.startOf("week");

        endDateTime = moment(beginDateTime);
        endDateTime.endOf("week");

    } else if (period === 2) {
        beginDateTime.startOf("month");

        endDateTime = moment(beginDateTime);
        endDateTime.endOf("month");

    } else if (period === 3) {
        beginDateTime.startOf("year");

        endDateTime = moment(beginDateTime);
        endDateTime.endOf("year");
    }

    return {beginDateTime: +beginDateTime, endDateTime: +endDateTime};
};
