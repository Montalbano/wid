import React from "react";
import ReactDOM from "react-dom";
import {Router, Route, IndexRedirect, useRouterHistory} from "react-router";
import {createHistory} from "history";

import UserStore from "./stores/UserStore";
import {verifyUser} from "./actions/UserActions";
import {clearMessage} from "./actions/MessageActions";

import App from "./components/App";
import Signin from "./components/Signin";
import Signup from "./components/Signup";
import Timeline from "./components/Timeline";
import Reports from "./components/Reports";
import Split from "./components/Split";
import Merge from "./components/Merge";
import ReportDetails from "./components/ReportDetails";
import Applications from "./components/Applications";
import Identities from "./components/Identities";
import Configuration from "./components/Configuration";
import Website from "./components/Website";
import Home from "./components/Home";
import Facets from "./components/Facets";
import Import from "./components/Import";
import Tags from "./components/Tags";
import Export from "./components/Export";
import Timebundle from "./components/Timebundle";
import ResetPassword from "./components/ResetPassword";
import Account from "./components/Account";
import Cgu from "./components/Cgu";
import Legals from "./components/Legals";

// Setup the locale for moment from the navigator locale
import moment from "moment";
moment.locale(navigator.language);

let container = document.createElement("div");
container.classList.add("container");
document.body.appendChild(container);

// Force to get user's state
let ready = new Promise((resolve) => {
    let call = () => {
        resolve();
        UserStore.removeChangeListener(call);
    };
    UserStore.addChangeListener(call);

    verifyUser();
});

let requireSecurityToken = (nextState, replace, cb) => {
    ready.catch().then(() => {
        if (!UserStore.isAuth() && window.online) {
            replace({
                pathname: "/site/signin",
                state: {nextPathname: nextState.location.pathname, test: true}
            });
        }
        cb();
    });
};

let history = useRouterHistory(createHistory)({
    basename: window.serverContext
});
history.listen(clearMessage);

ReactDOM.render(
    <Router history={history}>

        <Route component={Website} path="/site">
            <IndexRedirect to="home" />
            <Route component={Home} path="home"/>
            <Route component={Signin} path="signin"/>
            <Route component={Signup} path="signup"/>
            <Route component={ResetPassword} path="passwd/reset"/>
            <Route component={Cgu} path="cgu"/>
            <Route component={Legals} path="legals"/>
        </Route>

        <Route component={App} path="/">
            <IndexRedirect to="/site" />
            <Route component={Timeline} onEnter={requireSecurityToken} path="timeline">
                <Route component={Split} path="split/:begin/:end"/>
                <Route component={Merge} path="merge/:oldBegin/:oldEnd"/>
            </Route>
            <Route component={Reports} onEnter={requireSecurityToken} path="reports"/>
            <Route component={Facets} onEnter={requireSecurityToken} path="facets"/>
            <Route component={Facets} onEnter={requireSecurityToken} path="facets/**"/>
            <Route component={ReportDetails} onEnter={requireSecurityToken} path="report/:id"/>
            <Route component={ReportDetails} onEnter={requireSecurityToken} path="report/:id/:date"/>
            <Route component={Tags} onEnter={requireSecurityToken} path="tags"/>
            <Route component={Configuration} onEnter={requireSecurityToken} path="config">
                <IndexRedirect to="account" />
                <Route component={Account} path="account"/>
                <Route component={Applications} path="applications"/>
                <Route component={Identities} path="identities"/>
                <Route component={Import} path="import"/>
                <Route component={Export} path="export"/>
                <Route component={Timebundle} path="timebundle"/>
            </Route>
        </Route>
    </Router>
, container);
