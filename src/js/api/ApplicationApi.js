/* eslint strict: 0 */
// Needed in Node in order to use let
"use strict";

let uuid = require("uuid");
let userApi = require("./UserApi");
let eventApi = require("./EventApi");
let dbHelper = require("./DatabaseHelper");

module.exports.addIdentity = ({identifier, database}) => {
    return dbHelper.getGlobalDb()
            .then((db) => {
                return new Promise((resolve, reject) => {
                    let id = uuid.v1();

                    db.run("INSERT INTO identity (id, identifier, database) VALUES (?, ?, ?)",
                        id, identifier, database, (err) => {
                            if (err) {
                                reject(err);
                            } else {
                                resolve({id, identifier, database});
                            }
                        });
                });
            });
};

module.exports.createIdentity = ({token, identifier}) => {
    return userApi.getUser({token})
            .then((user) => {
                return module.exports.addIdentity({identifier, database: user.username});
            });
};

module.exports.removeIdentity = ({token, id}) => {
    return dbHelper.getGlobalDb()
            .then((db) => {

                return new Promise((resolve, reject) => {
                    userApi.getUser({token})
                    .then((user) => {
                        db.run("DELETE FROM identity WHERE id = ? AND database = ?", id, user.username, (err) => {
                            if (err) {
                                reject(err);
                            } else {
                                resolve(true);
                            }
                        });
                    })
                    .catch(reject);
                });
            });
};

module.exports.getIdentities = ({token}) => {
    return dbHelper.getGlobalDb()
            .then((db) => {

                return new Promise((resolve, reject) => {
                    userApi.getUser({token})
                    .then((user) => {
                        db.all("SELECT id, identifier FROM identity WHERE database = ?",
                            user.username, (err, rows) => {
                                if (err || !rows) {
                                    reject(err);
                                } else {
                                    resolve({rows});
                                }
                            });
                    })
                    .catch(reject);
                });
            });
};

module.exports.getDatabase = ({identifier}) => {
    return dbHelper.getGlobalDb()
            .then((db) => {

                return new Promise((resolve, reject) => {
                    db.get("SELECT database FROM identity WHERE identifier = ?",
                        identifier, (err, row) => {
                            if (err || !row) {
                                reject(err);
                            } else {
                                resolve(row);
                            }
                        });
                });
            });
};

module.exports.createKey = ({token, name}) => {
    return dbHelper.getGlobalDb()
            .then((db) => {

                return new Promise((resolve, reject) => {
                    let id = uuid.v1();
                    let key = uuid.v1();

                    userApi.getUser({token})
                    .then((user) => {
                        db.run("INSERT INTO application (id, name, database, key) VALUES (?, ?, ?, ?)",
                            id, name, user.username, key, (err) => {
                                if (err) {
                                    reject(err);
                                } else {
                                    resolve({id, name, key});
                                }
                            });
                    })
                    .catch(reject);
                });
            });
};

module.exports.removeKey = ({token, id}) => {
    return dbHelper.getGlobalDb()
            .then((db) => {

                return new Promise((resolve, reject) => {
                    userApi.getUser({token})
                    .then((user) => {
                        db.run("DELETE FROM application WHERE id = ? AND database = ?", id, user.username, (err) => {
                            if (err) {
                                reject(err);
                            } else {
                                resolve(true);
                            }
                        });
                    })
                    .catch(reject);
                });
            });
};

module.exports.getKeys = ({token}) => {
    return dbHelper.getGlobalDb()
            .then((db) => {

                return new Promise((resolve, reject) => {
                    userApi.getUser({token})
                    .then((user) => {
                        db.all("SELECT id, name, key FROM application WHERE database = ?",
                            user.username, (err, rows) => {
                                if (err || !rows) {
                                    reject(err);
                                } else {
                                    resolve({rows});
                                }
                            });
                    })
                    .catch(reject);
                });
            });
};

module.exports.isAutorize = ({key}) => {
    return dbHelper.getGlobalDb()
            .then((db) => {

                return new Promise((resolve, reject) => {
                    db.get("SELECT database FROM application where key = ?",
                        key, (err, row) => {
                            if (err || !row) {
                                reject(err);
                            } else {
                                resolve(row);
                            }
                        });
                });
            });
};

module.exports.createToken = ({key, identifier}) => {
    return module.exports.isAutorize({key})
            .then(() => {
                return module.exports.getDatabase({identifier})
                    .then(({database}) => {
                        let token = uuid.v4() + "@" + database;
                        return userApi.addToken(database, token)
                            .then(() => {
                                return token;
                            });
                    });
            });
};

module.exports.createTokenWithKey = ({key}) => {
    return module.exports.isAutorize({key})
            .then(({database}) => {
                let token = uuid.v4() + "@" + database;
                return userApi.addToken(database, token)
                    .then(() => {
                        return token;
                    });
            });
};

module.exports.onRepoWebHook = ({key, identifier, source, value}) => {
    return module.exports.createToken({key, identifier})
            .then((token) => {
                return eventApi.finish({token, source, value: value.join(" ")})
                        .then((event) => {
                            return userApi.removeToken({token})
                                    .then(() => {
                                        return {token, events: [event]};
                                    });
                        });
            });
};

module.exports.onEventsWebHook = ({data, key}) => {
    return module.exports.createTokenWithKey({key})
            .then((token) => {
                return eventApi.addAll({token, events: data})
                        .then(({events}) => {
                            return userApi.removeToken({token})
                                    .then(() => {
                                        return {token, events};
                                    });
                        });
            });
};

module.exports.onGitLabWebHook = ({data, key}) => {
    let email = data.user_email;
    let value = [data.project.name.toLowerCase()];

    let text = data.commits.reduce((prev, current) => {
        return prev + " " + current.message;
    }, "");
    let issues = text.match(/(#\d+)/g) || [];
    value = value.concat(issues);

    return module.exports.onRepoWebHook({key, identifier: email, source: "GITLAB", value});
};

module.exports.onGitHubWebHook = ({data, key}) => {
    let commits = data.commits;
    if (commits) {
        let email = commits[0].author.email;
        let value = [data.repository.name.toLowerCase()];

        let text = commits.reduce((prev, current) => {
            return prev + " " + current.message;
        }, "");
        let issues = text.match(/(#\d+)/g) || [];
        value = value.concat(issues);

        return module.exports.onRepoWebHook({key, identifier: email, source: "GITHUB", value});
    }

    // Ping call
    return Promise.resolve();
};
