/* eslint strict: 0 */
/* eslint new-cap: 0 */
/* eslint no-console: 0 */
// Needed in Node in order to use let
"use strict";

let dbHelper = require("./DatabaseHelper");
let dbMigration = require("./DatabaseMigration");
let uuid = require("uuid");
let userApi = require("./UserApi");

let moment = require("moment");
let fs = require("fs");
let globule = require("globule");
let unzip = require("unzip");
let rmdir = require("rimraf");
let streamifier = require("streamifier");
let path = require("path");
let Iconv = require("iconv").Iconv;

let dropImportDb = (db, drop) => {
    if (drop) {
        return dbHelper.dropAllTables(db).then(() => {
            return dbMigration.doSqls(db, dbMigration.schemaForUser);
        });
    }
    return Promise.resolve();
};

let execInserts = (db, content) => {
    let promises = [];
    let tableNames = Object.keys(content);
    tableNames.forEach((tableName) => {
        if (dbHelper.NO_IMPORT_TABLES.indexOf(tableName) === -1) {

            let rows = content[tableName];
            rows.forEach((row) => {
                let keys = Object.keys(row);
                let columns = keys.join(",");
                let values = keys.map((key) => {return row[key];});
                let params = keys.map(() => {return "?";}).join(",");

                promises.push(
                    new Promise((resolve, reject) => {
                        db.run(`INSERT INTO ${tableName} (${columns}) VALUES (${params})`, ...values, (err) => {
                            if (err) {
                                reject(err);
                            } else {
                                resolve();
                            }
                        });
                    })
                );
            });
        }
    });

    return Promise.all(promises);
};

module.exports.importDb = ({token, data, drop}) => {
    return new Promise((resolve, reject) => {
        userApi.isAuth({token})
        .then((db) => {
            let content;
            try {
                content = JSON.parse(data);
            } catch (err) {
                reject(err);
                return;
            }

            dbHelper.beginTransaction(db)
            .then(() => {
                dropImportDb(db, drop).then(() => {

                    dbHelper.endTransaction(db, execInserts(db, content))
                    .then(() => {
                        resolve(true);
                    })
                    .catch(reject);
                });
            })
            .catch(reject);
        })
        .catch(reject);
    });
};

let extractJtimerData = (baseDir) => {
    let data = {
        times: [],
        events: []
    };
    let filePaths = globule.find(baseDir + "*.task");

    let endDateTimes = new Map();
    let iconv = new Iconv("ISO-8859-1", "UTF-8");

    filePaths.forEach((filePath) => {
        let taskContent = fs.readFileSync(filePath);
        if (taskContent.length) {
            let taskLines = iconv.convert(taskContent).toString().split("\n");
            // console.log(filePath);

            let tasks = taskLines[1].split("/");
            tasks[0] = tasks[0].substring("Name: ".length);

            let projectFile = baseDir + taskLines[4].substring("Project: ".length) + ".project";
            let projectContent = fs.readFileSync(projectFile);
            if (projectContent.length) {

                // console.log(projectFile);
                let projectLines = iconv.convert(projectContent).toString().split("\n");
                let projectName = projectLines[1].substring("Name: ".length);
                if (projectName[0] === "#") {
                    projectName = projectName.substring(1);
                }
                tasks.unshift(projectName);

                let annMap = new Map();
                let annFile = baseDir + path.basename(filePath, ".task") + ".ann";
                try {
                    let annContent = fs.readFileSync(annFile);
                    if (annContent.length) {
                        // console.log(annFile);
                        let annLines = iconv.convert(annContent).toString().split("\n");

                        annLines.forEach((ann) => {
                            let match = ann.match(/([0-9]+) (.*)/);
                            let date = match[1];
                            let value = match[2];
                            let key = moment(date * 1000).format("YYYYMMDD");

                            let values = annMap.get(key);
                            if (!values) {
                                values = [];
                                annMap.set(key, values);
                            }

                            values.push(value);
                        });
                    }
                } catch (err) {
                    // Do nothing no annotation file exists
                }

                let times = taskLines.slice(6);
                times.forEach((time) => {
                    let [date, duration] = time.split(" ");

                    if (duration && duration !== "0") {
                        let beginDateTime = moment(endDateTimes.get(date) || date);

                        let endDateTime = moment(+beginDateTime).add(duration, "s");
                        endDateTimes.set(date, +endDateTime);

                        let annotations = annMap.get(date);
                        if (annotations) {
                            annotations.forEach((annotation) => {
                                data.events.push({
                                    id: uuid.v1(),
                                    beginDateTime: +beginDateTime,
                                    endDateTime: +beginDateTime,
                                    source: "NOTE",
                                    value: annotation
                                });
                            });
                        }

                        tasks.forEach((tag) => {
                            data.times.push({
                                id: uuid.v1(),
                                beginDateTime: +beginDateTime,
                                endDateTime: +endDateTime,
                                tag: tag.toLowerCase().replace(/ /g, "_").replace(/-/g, "_")
                            });
                        });
                    }
                });
            }
        }
    });

    return data;
};

let importData = (db, data) => {
    let insertTime = db.prepare("INSERT INTO time (id, beginDateTime, endDateTime, tag) VALUES (?, ?, ?, ?)");
    let insertEvent = db.prepare("INSERT INTO event (id, beginDateTime, endDateTime, source, value) VALUES (?, ?, ?, ?, ?)");

    let promises = [];

    data.times.forEach((time) => {
        let promise = new Promise((resolve, reject) => {
            insertTime.run(time.id, time.beginDateTime, time.endDateTime, time.tag, (err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        });

        promises.push(promise);
    });

    data.events.forEach((event) => {
        let promise = new Promise((resolve, reject) => {
            insertEvent.run(event.id, event.beginDateTime, event.endDateTime, event.source, event.value, (err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        });

        promises.push(promise);
    });

    return promises;
};

module.exports.importJtimer = ({token, data}) => {

    return new Promise((resolve, reject) => {
        let baseDir = "/tmp/data-" + token + "/";
        let stream = streamifier.createReadStream(data).pipe(unzip.Extract({path: baseDir}));

        stream.on("close", () => {

            userApi.isAuth({token})
            .then((db) => {
                db.run("BEGIN");

                let jtimerData = extractJtimerData(baseDir);
                let promises = importData(db, jtimerData);
                Promise.all(promises)
                .then(() => {
                    rmdir(baseDir, () => {
                        db.run("COMMIT", resolve.bind(null, true));
                    });
                })
                .catch((err) => {
                    rmdir(baseDir, () => {
                        db.run("ROLLBACK", reject.bind(null, err));
                    });
                });
            })
            .catch(reject);
        });
    });
};
