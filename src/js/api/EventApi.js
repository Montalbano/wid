/* eslint strict: 0 */
// Needed in Node in order to use let
"use strict";

let dbHelper = require("./DatabaseHelper");
let userApi = require("./UserApi");
let timeApi = require("./TimeApi");
let uuid = require("uuid");

module.exports.add = ({token, beginDateTime, endDateTime, source, value}) => {
    return new Promise((resolve, reject) => {
        userApi.isAuth({token})
        .then((db) => {
            let id = uuid.v1();

            db.run("INSERT INTO event (id, beginDateTime, endDateTime, source, value) VALUES (?, ?, ?, ?, ?)",
                id, beginDateTime, endDateTime, source, value, (err) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve({id, beginDateTime, endDateTime, source, value});
                    }
                });
        })
        .catch(reject);
    });
};

module.exports.addAll = ({token, events}) => {
    return new Promise((resolve, reject) => {
        userApi.isAuth({token})
        .then((db) => {

            dbHelper.beginTransaction(db)
            .then(() => {
                let insert = db.prepare("INSERT INTO event (id, beginDateTime, endDateTime, source, value) VALUES (?, ?, ?, ?, ?)");
                let promises = events.map((event) => {

                    return new Promise((resolveBis, rejectBis) => {

                        let id = uuid.v1();
                        let {beginDateTime, endDateTime, source, value} = event;
                        insert.run(id, beginDateTime, endDateTime, source, value, (errBis) => {
                            if (errBis) {
                                rejectBis(errBis);
                            } else {
                                resolveBis({id, beginDateTime, endDateTime, source, value});
                            }
                        });
                    });
                });

                dbHelper.endTransaction(db, Promise.all(promises))
                .then((result) => {
                    resolve({events: result});
                })
                .catch(reject);
            });
        })
        .catch(reject);
    });
};

module.exports.remove = ({token, id}) => {
    return new Promise((resolve, reject) => {
        userApi.isAuth({token})
        .then((db) => {
            db.run("DELETE FROM event WHERE id = ?", id, (err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(true);
                }
            });
        })
        .catch(reject);
    });
};

module.exports.getBeginDateTime = ({token}) => {
    return new Promise((resolve, reject) => {
        userApi.isAuth({token})
        .then((db) => {
            db.get("SELECT max(endDateTime) as beginDateTime FROM event", (err, row) => {
                if (err || !row) {
                    reject(err);
                } else {
                    resolve(row);
                }
            });
        })
        .catch(reject);
    });
};

module.exports.finish = ({token, source, value}) => {
    return new Promise((resolve, reject) => {
        Promise.all([
            module.exports.getBeginDateTime({token}),
            timeApi.getBeginDateTime({token})

        ]).then(([fromEvent, fromTime]) => {
            let beginDateFromEvent = fromEvent.beginDateTime;
            let beginDateFromTime = fromTime.beginDateTime || Date.now();

            let beginDateTime = beginDateFromEvent >= beginDateFromTime ? beginDateFromEvent : beginDateFromTime;
            let endDateTime = Date.now();

            module.exports.add({token, beginDateTime, endDateTime, source, value})
                .then(resolve).catch(reject);
        })
        .catch(reject);
    });
};

module.exports.start = ({token, beginDateTime = Date.now(), source, value, shift = 0}) => {
    let date = beginDateTime - shift;

    return module.exports.add({
        token,
        beginDateTime: date,
        endDateTime: date,
        source,
        value
    });
};

module.exports.end = ({token, endDateTime = Date.now(), source}) => {
    return new Promise((resolve, reject) => {
        userApi.isAuth({token})
        .then((db) => {

            db.run("UPDATE event SET endDateTime = ? WHERE source = ? AND beginDateTime = endDateTime AND beginDateTime <= ?",
                endDateTime, source, endDateTime, (err) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve({endDateTime, source});
                    }
                });
        })
        .catch(reject);
    });
};

module.exports.getAll = ({token, beginDateTime = 0, endDateTime = Number.MAX_VALUE}) => {
    return new Promise((resolve, reject) => {
        userApi.isAuth({token})
        .then((db) => {
            db.all("SELECT id, source, value, beginDateTime, endDateTime FROM event WHERE beginDateTime >= ? AND beginDateTime < ? ORDER BY beginDateTime DESC, endDateTime DESC",
                beginDateTime, endDateTime, (err, rows) => {
                    if (err || !rows) {
                        reject(err);
                    } else {
                        resolve({rows});
                    }
                });
        })
        .catch(reject);
    });
};

module.exports.getBySource = ({token, beginDateTime = 0, endDateTime = Number.MAX_VALUE, source}) => {
    return new Promise((resolve, reject) => {
        userApi.isAuth({token})
        .then((db) => {
            db.all("SELECT id, value, beginDateTime, endDateTime FROM event "
                + "WHERE beginDateTime >= ? AND beginDateTime < ? AND source = ? ORDER BY beginDateTime DESC, endDateTime DESC",
                beginDateTime, endDateTime, source, (err, rows) => {
                    if (err || !rows) {
                        reject(err);
                    } else {
                        resolve({rows});
                    }
                });
        })
        .catch(reject);
    });
};
