"use strict";

module.exports = class UnauthorizedError {

    constructor(msg) {
        this.msg = msg;
    }
};
