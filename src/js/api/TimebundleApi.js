/* eslint strict: 0 */
// Needed in Node in order to use let
"use strict";

let userApi = require("./UserApi");
let timeApi = require("./TimeApi");
let uuid = require("uuid");
let moment = require("moment");
let fetch = require("node-fetch");

module.exports.add = ({token, projectUrl, taskUrl, query}) => {
    return new Promise((resolve, reject) => {
        userApi.isAuth({token})
        .then((db) => {
            let id = uuid.v1();

            db.run("INSERT INTO timebundle (id, projectUrl, taskUrl, query) VALUES (?, ?, ?, ?)",
                id, projectUrl, taskUrl, query, (err) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve({id, projectUrl, taskUrl, query});
                    }
                });
        })
        .catch(reject);
    });
};

module.exports.save = ({token, id, query}) => {
    return new Promise((resolve, reject) => {
        userApi.isAuth({token})
        .then((db) => {

            db.run("UPDATE timebundle SET query = ? WHERE id = ?",
                query, id, (err) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(true);
                    }
                });
        })
        .catch(reject);
    });
};

module.exports.remove = ({token, id}) => {
    return new Promise((resolve, reject) => {
        userApi.isAuth({token})
        .then((db) => {
            db.run("DELETE FROM timebundle WHERE id = ?", id, (err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(true);
                }
            });
        })
        .catch(reject);
    });
};

module.exports.updateLastSync = ({token, refDate}) => {
    return new Promise((resolve, reject) => {
        userApi.isAuth({token})
        .then((db) => {

            db.run("UPDATE timebundle SET lastSync = ? WHERE lastSync >= ? AND lastSync is not null",
                refDate, refDate, (err) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(true);
                    }
                });
        })
        .catch(reject);
    });
};

timeApi.timeChange.on("change", (token, dates) => {
    let refDate = Math.min.apply(Math, dates);
    module.exports.updateLastSync({token, refDate});
});

module.exports.getProjects = ({token}) => {
    return new Promise((resolve, reject) => {
        userApi.isAuth({token})
        .then((db) => {
            db.all("SELECT DISTINCT projectUrl FROM timebundle", (err, rows) => {
                if (err || !rows) {
                    reject(err);
                } else {
                    resolve({rows});
                }
            });
        })
        .catch(reject);
    });
};

module.exports.getTasks = ({token, projectUrl}) => {
    return new Promise((resolve, reject) => {
        userApi.isAuth({token})
        .then((db) => {
            db.all("SELECT id, taskUrl, query, lastSync FROM timebundle WHERE projectUrl = ? ",
                projectUrl, (err, rows) => {

                    if (err || !rows) {
                        reject(err);
                    } else {
                        resolve({rows});
                    }
                });
        })
        .catch(reject);
    });
};

module.exports.getSyncTasks = ({token}) => {
    return new Promise((resolve, reject) => {
        userApi.isAuth({token})
        .then((db) => {
            db.all("SELECT taskUrl, query, lastSync FROM timebundle", (err, rows) => {
                if (err || !rows) {
                    reject(err);
                } else {
                    resolve({rows});
                }
            });
        })
        .catch(reject);
    });
};

module.exports.getJsonForTask = ({token, now, taskUrl, query, lastSync = 0}) => {
    return timeApi.getTimeForReport({token, beginDateTime: lastSync, endDateTime: now, query})
            .then(({rows}) => {
                let json = {
                    URL: taskUrl,
                    startDate: moment(lastSync || 0).format(moment.defaultFormat),
                    endDate: moment(now).format(moment.defaultFormat),
                    periods: []
                };

                let periods = json.periods;
                rows.forEach((item) => {
                    periods.push({
                        id: "wid-" + item.beginDateTime,
                        startDate: moment(item.beginDateTime).format(moment.defaultFormat),
                        duration: ~~((item.endDateTime - item.beginDateTime) / 1000),
                        info: item.tags
                    });
                });

                return json;
            });
};

module.exports.syncTaskOnServer = ({taskUrl, json}) => {
    return fetch(taskUrl, {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(json)
    })
    .then((response) => {
        if (response.status >= 400) {
            return Promise.reject();
        }
        return Promise.resolve();
    });
};

module.exports.updateTaskSyncDate = ({token, taskUrl, now}) => {
    return new Promise((resolve, reject) => {
        userApi.isAuth({token})
        .then((db) => {

            db.run("UPDATE timebundle SET lastSync = ? WHERE taskUrl = ?",
                now, taskUrl, (err) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(true);
                    }
                });
        })
        .catch(reject);
    });
};

module.exports.syncTask = ({token, now, taskUrl, query, lastSync = 0}) => {
    return module.exports.getJsonForTask({token, now, taskUrl, query, lastSync})
            .then((json) => {
                return module.exports.syncTaskOnServer({taskUrl, json});
            })
            .then(() => {
                return module.exports.updateTaskSyncDate({token, taskUrl, now});
            });
};

module.exports.syncTasks = ({token, now, rows}) => {
    if (rows.length) {
        let item = rows.shift();
        return module.exports.syncTask(Object.assign(item, {
            token,
            now: +now
        }))
        .then(() => {
            module.exports.syncTasks({token, now, rows});
        });
    }

    return Promise.resolve();
};

module.exports.sync = ({token, now}) => {
    return module.exports.getSyncTasks({token, now: +now})
            .then(({rows}) => {
                return module.exports.syncTasks({token, now, rows});
            });
};
