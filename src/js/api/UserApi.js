/* eslint strict: 0 */
/* eslint no-process-env: 0 */
// Needed in Node in order to use let
"use strict";

let uuid = require("uuid");
let UnauthorizedError = require("./UnauthorizedError");
let timeApi = require("./TimeApi");
let applicationApi = require("./ApplicationApi");
let dbHelper = require("./DatabaseHelper");
let crypto = require("crypto");
let nodemailer = require("nodemailer");

let serverMail = process.env.WID_SERVER_MAIL;
let transport = nodemailer.createTransport(serverMail);
let serverFrom = process.env.WID_SERVER_MAIL_FROM;

let cryptPassword = (passwd, salt) => {
    salt = salt || crypto.randomBytes(128).toString("base64");
    let crypted = crypto.pbkdf2Sync(passwd, salt, 10000, 512, "sha512");
    return ("sha512$" + salt + "$" + crypted.toString("base64"));
};

let verifyPassword = (db, passwd) => {
    return new Promise((resolve, reject) => {
        db.get("SELECT id, username, email, passwd, generatedPasswd FROM user", (err, row) => {
            if (err || !row) {
                reject(err);
            } else {
                let mainSplit = row.passwd.split("$");
                let mainPasswd = mainSplit[2];

                // Plain password
                if (!mainPasswd && row.passwd === passwd) {
                    resolve(row);
                    return;
                }

                // Main password
                let saltMainPasswd = mainSplit[1];
                let cryptPasswd = cryptPassword(passwd, saltMainPasswd);
                if (cryptPasswd === row.passwd) {
                    resolve(row);
                    return;
                }

                // Generated password
                if (row.generatedPasswd) {
                    let generateSplit = row.generatedPasswd.split("$");
                    let generatedSalt = generateSplit[1];
                    let generatedDate = parseInt(generateSplit[3], 10);

                    if (Date.now() - generatedDate <= 24 * 60 * 60 * 1000) { // 24h
                        cryptPasswd = cryptPassword(passwd, generatedSalt) + "$" + generatedDate;

                        if (cryptPasswd === row.generatedPasswd) {
                            resolve(row);
                            return;
                        }
                    }
                }

                reject();
            }
        });
    });
};

module.exports.signUp = ({username, email, passwd}) => {
    return new Promise((resolve, reject) => {
        dbHelper.createDb(username)
        .then((db) => {

            db.run("INSERT INTO user (id, username, email, passwd) VALUES (?, ?, ?, ?)", uuid.v1(), username, email, cryptPassword(passwd), (err) => {
                if (err) {
                    reject(err);
                } else {
                    applicationApi.addIdentity({identifier: email, database: username})
                    .then(resolve)
                    .catch(reject);
                }
            });
        });
    });
};

let addBeginDateTimeForUser = (token, user) => {
    return timeApi.getBeginDateTime({token})
            .catch(() => {
                return {beginDateTime: null};
            })
            .then((beginDateTime) => {
                return Object.assign({token}, user, beginDateTime);
            });
};

module.exports.addToken = (username, token) => {
    return dbHelper.getDbByName(username)
            .then((db) => {

                return new Promise((resolve, reject) => {
                    db.run("INSERT INTO token (id) VALUES (?)", token, (err) => {
                        if (err) {
                            reject(err);
                        } else {
                            resolve();
                        }
                    });
                });
            });
};

module.exports.signIn = ({username, passwd}) => {
    return dbHelper.getDbByName(username)
            .then((db) => {
                return verifyPassword(db, passwd);
            })
            .then((user) => {
                let token = uuid.v4() + "@" + username;
                return module.exports.addToken(username, token)
                        .then(() => {
                            return addBeginDateTimeForUser(token, user);
                        });
            });
};

module.exports.generatePassword = ({email}) => {
    let newPasswd;

    return applicationApi.getDatabase({identifier: email})
    .then(({database}) => {
        return dbHelper.getDbByName(database);
    })
    .then((db) => {
        newPasswd = Math.random().toString(36).slice(-8);

        return new Promise((resolve, reject) => {
            db.run("UPDATE user SET generatedPasswd = ?", cryptPassword(newPasswd) + "$" + Date.now(), (err) => {
                if (err) {
                    reject();
                } else {
                    resolve();
                }
            });
        });
    })
    .then(() => {
        return new Promise((resolve, reject) => {
            transport.sendMail({
                from: serverFrom,
                to: email,
                subject: "New password for Wid",
                text: `Your new password is ${newPasswd}. The password is valid 24h\nRegards.\nWid teams`
            }, (err) => {
                if (err) {
                    reject();
                } else {
                    resolve(true);
                }
            });
        });
    });
};

module.exports.changePassword = ({token, oldPasswd, newPasswd}) => {
    let database;
    return new Promise((resolve, reject) => {
        module.exports.isAuth({token})
        .then((db) => {
            database = db;
            return verifyPassword(db, oldPasswd);
        })
        .then(() => {
            database.run("UPDATE user SET passwd = ?", cryptPassword(newPasswd), (err) => {
                if (err) {
                    reject();
                } else {
                    resolve(true);
                }
            });
        })
        .catch(reject);
    });
};

module.exports.isAuth = ({token}) => {
    return dbHelper.getDb(token)
            .then((db) => {
                if (db === token) {
                    return db;
                }

                return new Promise((resolve, reject) => {
                    db.get("SELECT 1 FROM token where id = ?", token, (err, row) => {
                        if (err || !row) {
                            reject(new UnauthorizedError(err));
                        } else {
                            resolve(db);
                        }
                    });
                });
            });
};

module.exports.getUser = ({token}) => {
    return new Promise((resolve, reject) => {
        module.exports.isAuth({token})
        .then((db) => {
            db.get("SELECT id, username, email FROM user", (err, row) => {
                if (err || !row) {
                    reject(err);
                } else {
                    return addBeginDateTimeForUser(token, row).then(resolve);
                }
                return null;
            });
        })
        .catch(reject);
    });
};

module.exports.removeToken = module.exports.signOut = ({token}) => {
    return new Promise((resolve, reject) => {
        module.exports.isAuth({token})
        .then((db) => {
            db.run("DELETE FROM token WHERE id = ?", token, (err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(true);
                }
            });
        })
        .catch(reject);
    });
};

module.exports.exportDb = ({token}) => {
    return new Promise((resolve, reject) => {
        module.exports.isAuth({token})
        .then((db) => {

            return dbHelper.getAllTableNames(db)
                    .then((rows) => {
                        let promises = [];

                        rows.forEach(({tableName}) => {
                            if (dbHelper.NO_EXPORT_TABLES.indexOf(tableName) === -1) {
                                promises.push(module.exports.exportTable({token, tableName}));
                            }
                        });

                        Promise.all(promises).then((contents) => {
                            resolve({
                                name: "wid-" + db.name + "-" + Date.now(),
                                file: "{" + contents.join(",\n") + "}"
                            });
                        }, reject);
                    })
                    .catch(reject);
        })
        .catch(reject);
    });
};

module.exports.exportTable = ({token, tableName}) => {
    return new Promise((resolve, reject) => {
        module.exports.isAuth({token})
            .then((db) => {
                db.all("SELECT * FROM " + tableName, (err, rows) => {
                    if (err) {
                        reject(err);
                    } else {
                        let content = JSON.stringify(rows, null, " ");
                        resolve("\"" + tableName + "\":" + content);
                    }
                });
            })
            .catch(reject);
    });
};
