/* eslint strict: 0 */
// Needed in Node in order to use let
"use strict";

let uuid = require("uuid");
let userApi = require("./UserApi");
let timeApi = require("./TimeApi");
let eventApi = require("./EventApi");
let moment = require("moment");

// const PERIOD_TYPE [
//     "TODAY",
//     "WEEK",
//     "MONTH",
//     "YEAR"
// ];

module.exports.add = ({token, name, tags, period, widget = 0}) => {
    return new Promise((resolve, reject) => {
        userApi.isAuth({token})
        .then((db) => {

            let id = uuid.v1();
            db.run("INSERT INTO report (id, name, tags, period, widget) VALUES (?, ?, ?, ?, ?)",
                id, name, tags, period, widget, (err) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve({id});
                    }
                });
        })
        .catch(reject);
    });
};

module.exports.remove = ({token, id}) => {
    return new Promise((resolve, reject) => {
        userApi.isAuth({token})
        .then((db) => {
            db.run("DELETE FROM report WHERE id = ?", id, (err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(true);
                }
            });
        })
        .catch(reject);
    });
};

module.exports.getAll = ({token}) => {
    return new Promise((resolve, reject) => {
        userApi.isAuth({token})
        .then((db) => {
            db.all("SELECT id, name, tags, period, widget FROM report ORDER BY name DESC",
                    (err, rows) => {
                        if (err || !rows) {
                            reject(err);
                        } else {
                            resolve({rows});
                        }
                    });
        })
        .catch(reject);
    });
};

module.exports.getAllWidgets = ({token}) => {
    return new Promise((resolve, reject) => {
        userApi.isAuth({token})
        .then((db) => {
            db.all("SELECT id, name, tags, period, widget FROM report WHERE widget = 1 ORDER BY name DESC",
                    (err, rows) => {
                        if (err || !rows) {
                            reject(err);
                        } else {
                            resolve({rows});
                        }
                    });
        })
        .catch(reject);
    });
};

module.exports.get = ({token, id}) => {
    return new Promise((resolve, reject) => {
        userApi.isAuth({token})
        .then((db) => {
            db.get("SELECT * FROM report WHERE id = ? ORDER BY name DESC",
                    id, (err, row) => {
                        if (err || !row) {
                            reject(err);
                        } else {
                            resolve(row);
                        }
                    });
        })
        .catch(reject);
    });
};

module.exports.getTotalTime = ({token, id, beginDateTime, endDateTime}) => {
    return module.exports.get({token, id})
        .then((report) => {
            let parameters = {
                token,
                beginDateTime: +beginDateTime,
                endDateTime: +endDateTime,
                query: report.tags
            };
            return timeApi.getTotalTimeForReport(parameters);
        });
};

module.exports.getDetailsTime = ({token, id, beginDateTime, endDateTime, tags}) => {
    return module.exports.get({token, id})
        .then((report) => {
            let promises = [];
            let parameters = {
                token,
                beginDateTime: +beginDateTime,
                endDateTime: +endDateTime,
                query: report.tags,
                tags: tags,
                source: "NOTE"
            };

            let promiseTime = timeApi.getTimeForReport(parameters);
            promises.push(promiseTime);

            let promiseFacet = timeApi.facetExport(parameters);
            promises.push(promiseFacet);

            let promiseNotes = eventApi.getBySource(parameters);
            promises.push(promiseNotes);

            let promiseTotal = timeApi.getTotalTimeForReport(parameters);
            promises.push(promiseTotal);

            let period = report.period;
            let number;
            let unit;
            if (period === 0) {
                number = 1;
                unit = "days";

            } else if (period === 1) {
                number = 7;
                unit = "days";

            } else if (period === 2) {
                number = 1;
                unit = "months";

            } else if (period === 3) {
                number = 1;
                unit = "years";
            }

            let beginDate = moment(parameters.beginDateTime);
            for (let i = 0; i < 11; i++) {
                let endDate = +beginDate;
                beginDate = beginDate.subtract(number, unit);

                parameters.beginDateTime = +beginDate;
                parameters.endDateTime = endDate;

                let historyTime = timeApi.getTotalTimeForReport(parameters);
                promises.push(historyTime);
            }

            return Promise.all(promises)
                    .then((result) => {
                        return {
                            report: {
                                name: report.name,
                                period: report.period,
                                tags: report.tags
                            },
                            total: result[3].time,
                            details: result[0].rows,
                            facets: result[1],
                            notes: result[2].rows,
                            history: result.slice(3).map((v) => {return v.time;})
                        };
                    });
        });
};

module.exports.getWidgetsTime = ({token, beginDateTime, endDateTime}) => {
    return module.exports.getAllWidgets({token})
            .then(({rows}) => {
                let promises = rows.map((report) => {
                    let parameters = {
                        token,
                        beginDateTime: +beginDateTime,
                        endDateTime: +endDateTime,
                        query: report.tags
                    };
                    return timeApi.getTotalTimeForReport(parameters);
                });

                return Promise.all(promises)
                        .then((times) => {
                            let widgets = times.map((time, index) => {
                                let report = rows[index];
                                return Object.assign(time, {name: report.name, period: report.period});
                            });

                            return {widgets};
                        });
            });
};
