/* eslint strict: 0 */
// Needed in Node in order to use let
"use strict";

let sqlite3 = require("sqlite3").verbose();
let baseDir = process.cwd() + "/data/";
let dbMigration = require("./DatabaseMigration");
let fs = require("fs");

function isCreated(dbPath) {
    if (global.dev) {
        return Promise.resolve(false);
    }

    return new Promise((resolve) => {
        fs.access(dbPath, fs.F_OK, function(err) {
            return resolve(err === null);
        });
    });
}

function exists(dbPath) {
    if (global.dev) {
        return new Promise((resolve, reject) => {
            if (global.memoryDb) {
                resolve();
            } else {
                reject();
            }
        });
    }

    return new Promise((resolve, reject) => {
        fs.access(dbPath, fs.F_OK, function(err) {
            if (err === null) {
                resolve();
            } else {
                reject();
            }
        });
    });
}

module.exports.createDb = (username) => {
    global.memoryDb = global.dev ? new sqlite3.Database(":memory:") : null;
    let db = global.memoryDb || new sqlite3.Database(baseDir + username + ".sqlite");

    return dbMigration.doSqls(db, dbMigration.schemaForUser)
    .then(() => {
        return db;
    });
};

module.exports.getName = (token) => {
    return token.split("@")[1];
};

module.exports.getDbPath = (name) => {
    return baseDir + name + ".sqlite";
};

module.exports.getDbByName = (name) => {
    let dbPath = module.exports.getDbPath(name);
    return exists(dbPath)
            .then(() => {
                let db = global.memoryDb || new sqlite3.Database(dbPath);
                db.name = name;

                return dbMigration.applyMigration(db, dbMigration.migrationsForUser)
                .then(() => {
                    return db;
                });
            });
};

module.exports.getDb = (token) => {
    if (typeof token !== "string") {
        return dbMigration.applyMigration(token, dbMigration.migrationsForUser)
        .then(() => {
            return token;
        });
    }

    let name = module.exports.getName(token);
    let dbPath = module.exports.getDbPath(name);
    return exists(dbPath)
            .then(() => {
                let db = global.memoryDb || new sqlite3.Database(dbPath);
                db.name = name;

                return dbMigration.applyMigration(db, dbMigration.migrationsForUser)
                .then(() => {
                    return db;
                });
            });
};

let globalDBPromise = null;
module.exports.getGlobalDb = () => {
    if (!globalDBPromise) {
        let globalDB = null;

        let dbPath = baseDir + "/global.sqlite";
        globalDBPromise = isCreated(dbPath)
                    .then((created) => {
                        globalDB = global.dev ? new sqlite3.Database(":memory:") : new sqlite3.Database(dbPath);

                        if (!created) {
                            return dbMigration.doSqls(globalDB, dbMigration.schemaForGlobal);
                        }

                        return Promise.resolve();
                    }).then(() => {
                        return dbMigration.applyMigration(globalDB, dbMigration.migrationsForGlobal);
                    })
                    .then(() => {
                        return globalDB;
                    });
    }

    return globalDBPromise;
};

module.exports.beginTransaction = (token) => {
    return module.exports.getDb(token)
    .then((db) => {
        db.run("BEGIN");
        return db;
    });
};

module.exports.endTransaction = (db, promise) => {
    return promise.then((result) => {
        return new Promise((resolve, reject) => {
            db.run("COMMIT", (err) => {
                if (err) {
                    reject(err);
                }

                resolve(result);
            });
        });
    })
    .catch((err) => {
        return new Promise((resolve, reject) => {
            db.run("ROLLBACK", (error) => {
                return reject(err || error);
            });
        });
    });
};

module.exports.getAllTableNames = (db) => {
    return new Promise((resolve, reject) => {
        db.all("SELECT name as tableName FROM sqlite_master WHERE type='table'", (err, rows) => {
            if (err) {
                reject(err);
            } else {
                resolve(rows);
            }
        });
    });
};

module.exports.dropTable = (db, tableName) => {
    return new Promise((resolve, reject) => {
        db.run("DROP TABLE IF EXISTS " + tableName, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
};

module.exports.NO_EXPORT_TABLES = ["user", "token"];
module.exports.NO_IMPORT_TABLES = ["user", "token", "version"];

module.exports.dropAllTables = (db) => {
    return new Promise((resolve, reject) => {
        return module.exports.getAllTableNames(db)
                .then((rows) => {
                    let promises = [];

                    rows.forEach(({tableName}) => {
                        if (module.exports.NO_IMPORT_TABLES.indexOf(tableName) === -1) {
                            promises.push(module.exports.dropTable(db, tableName));
                        }
                    });

                    Promise.all(promises).then(resolve, reject);
                })
                .catch(reject);
    });
};
