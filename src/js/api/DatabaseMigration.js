/* eslint strict: 0 */
/* eslint no-console: 0 */
// Needed in Node in order to use let
"use strict";

module.exports.migrationsForUser = [
    // Version 0
    [
        "PRAGMA journal_mode=WAL",

        // Add version
        "CREATE TABLE IF NOT EXISTS version (" +
            "value INTEGER PRIMARY KEY" +
        ")",
        "INSERT OR REPLACE INTO version (value) VALUES (0)",

        // Add timebundle
        "CREATE TABLE IF NOT EXISTS timebundle (" +
            "id TEXT PRIMARY KEY, " +
            "projectUrl TEXT NOT NULL, " +
            "taskUrl TEXT UNIQUE NOT NULL, " +
            "query TEXT NOT NULL," +
            "lastSync INTEGER" +
        ")",
        "CREATE INDEX IF NOT EXISTS IDX_TIMEBUNDLE_PROJECTURL ON timebundle (projectUrl)",
        "CREATE INDEX IF NOT EXISTS IDX_TIMEBUNDLE_TASKURL ON timebundle (taskUrl)",
        "CREATE INDEX IF NOT EXISTS IDX_LASTSYNC ON timebundle (lastSync)"
    ],
    [
        "ALTER TABLE user ADD COLUMN generatedPasswd TEXT",
        "UPDATE version SET value = 1"
    ]
];

module.exports.migrationsForGlobal = [
    // Version 0
    [
        "PRAGMA journal_mode=WAL",

        // Add version
        "CREATE TABLE IF NOT EXISTS version (" +
            "value INTEGER PRIMARY KEY" +
        ")",
        "INSERT OR REPLACE INTO version (value) VALUES (0)"
    ],
    // Version 1
    [
        // Drop table
        "DROP TABLE IF EXISTS application",
        "CREATE TABLE IF NOT EXISTS application (" +
                "id TEXT PRIMARY KEY, " +
                "name TEXT UNIQUE, " +
                "database TEXT, " +
                "key TEXT" +
            ")",
        "UPDATE version SET value = 1"
    ]
];

module.exports.schemaForUser = [
    "PRAGMA journal_mode=WAL",

    "CREATE TABLE IF NOT EXISTS user (" +
            "id TEXT PRIMARY KEY, " +
            "username TEXT UNIQUE, " +
            "email TEXT UNIQUE, " +
            "passwd TEXT," +
            "generatedPasswd TEXT" +
        ")",

    "CREATE TABLE IF NOT EXISTS token (" +
            "id TEXT PRIMARY KEY" +
        ")",

    "CREATE TABLE IF NOT EXISTS event (" +
            "id TEXT PRIMARY KEY, " +
            "beginDateTime INTEGER, " +
            "endDateTime INTEGER, " +
            "source TEXT NOT NULL, " +
            "value TEXT" +
        ")",

    "CREATE INDEX IF NOT EXISTS IDX_EVENT_SOURCE ON event (source)",
    "CREATE INDEX IF NOT EXISTS IDX_EVENT_BEGIN_DATE_TIME ON event (beginDateTime)",
    "CREATE INDEX IF NOT EXISTS IDX_EVENT_END_DATE_TIME ON event (endDateTime)",
    "CREATE INDEX IF NOT EXISTS IDX_EVENT_DATE_TIME ON event (beginDateTime, endDateTime)",

    "CREATE TABLE IF NOT EXISTS report (" +
            "id TEXT PRIMARY KEY, " +
            "name TEXT, " +
            "tags TEXT, " +
            "period INTEGER, " +
            "widget INTEGER" +
        ")",

    "CREATE TABLE IF NOT EXISTS time (" +
            "id TEXT PRIMARY KEY, " +
            "beginDateTime INTEGER, " +
            "endDateTime INTEGER, " +
            "tag TEXT" +
        ")",

    "CREATE INDEX IF NOT EXISTS IDX_TIME_BEGIN_DATE_TIME ON time (beginDateTime)",
    "CREATE INDEX IF NOT EXISTS IDX_TIME_END_DATE_TIME ON time (endDateTime)",
    "CREATE INDEX IF NOT EXISTS IDX_TIME_DATE_TIME ON time (beginDateTime, endDateTime)",
    "CREATE INDEX IF NOT EXISTS IDX_TIME_TAG ON time (tag)",

    "CREATE TABLE IF NOT EXISTS version (" +
            "value INTEGER PRIMARY KEY" +
        ")",
    "INSERT OR REPLACE INTO version (value) VALUES (" + (module.exports.migrationsForUser.length - 1) + ")",

    "CREATE TABLE IF NOT EXISTS timebundle (" +
            "id TEXT PRIMARY KEY, " +
            "projectUrl TEXT NOT NULL, " +
            "taskUrl TEXT UNIQUE NOT NULL, " +
            "query TEXT NOT NULL," +
            "lastSync INTEGER" +
        ")",

    "CREATE INDEX IF NOT EXISTS IDX_TIMEBUNDLE_PROJECTURL ON timebundle (projectUrl)",
    "CREATE INDEX IF NOT EXISTS IDX_TIMEBUNDLE_TASKURL ON timebundle (taskUrl)",
    "CREATE INDEX IF NOT EXISTS IDX_LASTSYNC ON timebundle (lastSync)"
];

module.exports.schemaForGlobal = [
    "PRAGMA journal_mode=WAL",

    "CREATE TABLE IF NOT EXISTS identity (" +
            "id TEXT PRIMARY KEY, " +
            "identifier TEXT UNIQUE, " +
            "database TEXT" +
        ")",

    "CREATE TABLE IF NOT EXISTS application (" +
            "id TEXT PRIMARY KEY, " +
            "name TEXT UNIQUE, " +
            "database TEXT, " +
            "key TEXT" +
        ")",

    "CREATE TABLE IF NOT EXISTS version (" +
            "value INTEGER PRIMARY KEY" +
        ")",
    "INSERT OR REPLACE INTO version (value) VALUES (" + (module.exports.migrationsForGlobal.length - 1) + ")"
];

let users = new Map();

function getVersion(db) {
    return new Promise((resolve) => {
        db.get("SELECT value FROM version", (err, row) => {
            if (err) {
                resolve(-1);
            } else {
                resolve(row.value);
            }
        });
    });
}

module.exports.doSqls = (db, sqls) => {
    if (!sqls) {
        return Promise.resolve();
    }

    let promises;
    db.serialize(function() {
        promises = sqls.map((sql) => {
            return new Promise((resolve, reject) => {
                // console.log(">", sql);
                db.run(sql, (err) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                });
            });
        });
    });

    return Promise.all(promises);
};

function migrate(db, migrations, currentVersion) {
    if (currentVersion < migrations.length - 1) {
        let sqls = migrations[currentVersion + 1];
        console.log(`Migration from ${currentVersion} to ${currentVersion + 1}`);
        return module.exports.doSqls(db, sqls)
                .then(() => {
                    return migrate(db, migrations, currentVersion + 1);
                });
    }

    return Promise.resolve();
}

module.exports.applyMigration = (db, migrations) => {
    let fileName = db.name || db.filename;

    let promise = users.get(fileName);
    if (!promise) {
        promise = getVersion(db)
                    .then((currentVersion) => {
                        return migrate(db, migrations, currentVersion);
                    });
        users.set(fileName, promise);
    }

    return promise;
};
