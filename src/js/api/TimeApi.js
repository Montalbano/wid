/* eslint strict: 0 */
// Needed in Node in order to use let
"use strict";

let uuid = require("uuid");
let userApi = require("./UserApi");
let eventApi = require("./EventApi");
let dbHelper = require("./DatabaseHelper");
let EventEmitter = require("events");

// Event use to update last synch in Timebundle
module.exports.timeChange = new EventEmitter();

module.exports.add = ({token, beginDateTime, endDateTime, tags, note}) => {
    let values = Array.isArray(tags) ? tags : [tags];
    values = Array.from(new Set(values)); // remove doublon

    return new Promise((resolve, reject) => {
        let database;
        let beginDateValue;

        userApi.isAuth({token})
        .then((db) => {
            database = db;

            let beginDatePromise;
            if (beginDateTime) {
                beginDatePromise = Promise.resolve({beginDateTime});
            } else {
                beginDatePromise = module.exports.getBeginDateTime({token});
            }

            return beginDatePromise;
        })
        .then(({beginDateTime: beginDate}) => {
            beginDateValue = beginDate;

            let promiseForNote = Promise.resolve();
            if (note) {
                promiseForNote = eventApi.add({
                    token,
                    beginDateTime: beginDateValue,
                    endDateTime: beginDateValue,
                    source: "NOTE",
                    value: note
                });
            }

            return promiseForNote;
        })
        .then(() => {
            return module.exports.getByEndDateTime({token, endDateTime: beginDateValue});
        })
        .then(({rows}) => {
            // Test if the sames tags between previous tags
            let intersection = rows.filter((time) => {
                return tags.indexOf(time.tag) !== -1;
            });

            if (intersection.length === values.length && intersection.length === rows.length) {
                // Update previous tag
                database.run("UPDATE time SET endDateTime = ? WHERE endDateTime = ?",
                    +endDateTime, beginDateValue, (err) => {
                        if (err) {
                            reject(err);
                        } else {
                            resolve(true);
                        }
                    });

            } else {
                dbHelper.beginTransaction(database)
                .then(() => {
                    let insert = database.prepare("INSERT INTO time (id, beginDateTime, endDateTime, tag) VALUES (?, ?, ?, ?)");
                    let promises = values.map((tag) => {

                        return new Promise((resolveBis, rejectBis) => {

                            let timeId = uuid.v1();
                            insert.run(timeId, beginDateValue, endDateTime, tag, (errBis) => {
                                if (errBis) {
                                    rejectBis(errBis);
                                } else {
                                    resolveBis(timeId);
                                }
                            });
                        });
                    });

                    dbHelper.endTransaction(database, Promise.all(promises))
                    .then(() => {
                        module.exports.timeChange.emit("change", token, [beginDateValue]);
                        resolve(true);
                    })
                    .catch(reject);
                });
            }
        })
        .catch(reject);
    });
};

module.exports.removeOnDate = ({token, beginDateTime, endDateTime}) => {
    return new Promise((resolve, reject) => {
        userApi.isAuth({token})
        .then((db) => {
            db.run("DELETE FROM time WHERE beginDateTime = ? AND endDateTime = ?", beginDateTime, endDateTime, (err) => {
                if (err) {
                    reject(err);
                } else {
                    module.exports.timeChange.emit("change", token, [beginDateTime]);
                    resolve(true);
                }
            });
        })
        .catch(reject);
    });
};

module.exports.removeOnDateAndTag = ({token, beginDateTime, endDateTime, tags}) => {
    return new Promise((resolve, reject) => {
        userApi.isAuth({token})
        .then((db) => {
            db.run("DELETE FROM time WHERE beginDateTime = ? AND endDateTime = ? AND tag IN (?" + ", ?".repeat(tags.length - 1) + ")",
            beginDateTime, endDateTime, ...tags, (err) => {
                if (err) {
                    reject(err);
                } else {
                    module.exports.timeChange.emit("change", token, [beginDateTime]);
                    resolve(true);
                }
            });
        })
        .catch(reject);
    });
};

module.exports.createOrUpdate = ({token, beginDateTime, endDateTime, tags, note}) => {
    return module.exports.removeOnDate({token, beginDateTime, endDateTime})
            .then(() => {
                return module.exports.add({token, beginDateTime, endDateTime, tags, note});
            });
};

module.exports.modifyDate = ({token, oldBeginDateTime, oldEndDateTime, newBeginDateTime, newEndDateTime}) => {
    return new Promise((resolve, reject) => {
        userApi.isAuth({token})
        .then((db) => {
            db.run("UPDATE time SET beginDateTime = ?, endDateTime = ? WHERE beginDateTime = ? AND endDateTime = ?",
                    newBeginDateTime, newEndDateTime, oldBeginDateTime, oldEndDateTime, (err) => {
                        if (err) {
                            reject(err);
                        } else {
                            module.exports.timeChange.emit("change", token, [oldBeginDateTime, newBeginDateTime]);
                            resolve(true);
                        }
                    });
        })
        .catch(reject);
    });
};

module.exports.getDateMergeUp = ({token, oldEndDateTime}) => {
    return new Promise((resolve, reject) => {
        userApi.isAuth({token})
        .then((db) => {
            db.get("SELECT min(beginDateTime) as beginDateTime, endDateTime FROM time WHERE beginDateTime >= ?", +oldEndDateTime, (err, row) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(row);
                }
            });
        })
        .catch(reject);
    });
};

module.exports.getDateMergeDown = ({token, oldBeginDateTime}) => {
    return new Promise((resolve, reject) => {
        userApi.isAuth({token})
        .then((db) => {
            db.get("SELECT beginDateTime, max(endDateTime) as endDateTime FROM time WHERE endDateTime <= ?", +oldBeginDateTime, (err, row) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(row);
                }
            });
        })
        .catch(reject);
    });
};

module.exports.mergeUp = ({token, oldBeginDateTime, oldEndDateTime, overwrite}) => {
    return module.exports.getDateMergeUp({token, oldEndDateTime})
    .then(({beginDateTime, endDateTime}) => {
        return module.exports.mergeDate({
            token,
            oldBeginDateTime,
            oldEndDateTime,
            refBeginDateTime: beginDateTime,
            refEndDateTime: endDateTime,
            overwrite
        });
    });
};

module.exports.mergeDown = ({token, oldBeginDateTime, oldEndDateTime, overwrite}) => {
    return module.exports.getDateMergeDown({token, oldBeginDateTime})
    .then(({beginDateTime, endDateTime}) => {
        return module.exports.mergeDate({
            token,
            oldBeginDateTime,
            oldEndDateTime,
            refBeginDateTime: beginDateTime,
            refEndDateTime: endDateTime,
            overwrite
        });
    });
};

module.exports.mergeDuplicateTags = ({token, beginDateTime, endDateTime}) => {
    return new Promise((resolve, reject) => {
        userApi.isAuth({token})
        .then((db) => {
            db.run("DELETE FROM time WHERE beginDateTime >= ? AND endDateTime <= ? AND id NOT IN (SELECT min(id) FROM time "
            + "WHERE beginDateTime >= ? AND endDateTime <= ? GROUP BY beginDateTime, endDateTime, tag)",
            +beginDateTime, +endDateTime, +beginDateTime, +endDateTime, (err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(true);
                }
            });
        })
        .catch(reject);
    });
};

module.exports.mergeDate = ({token, oldBeginDateTime, oldEndDateTime, refBeginDateTime, refEndDateTime, overwrite}) => {
    return new Promise((resolve, reject) => {
        if (!refBeginDateTime || !refEndDateTime) {
            resolve(false);
            return;
        }

        userApi.isAuth({token})
        .then((db) => {

            let newBeginDateTime = oldBeginDateTime;
            let newEndDateTime = oldEndDateTime;

            if (+oldEndDateTime === +refBeginDateTime) {
                newEndDateTime = refEndDateTime;

            } else if (+oldBeginDateTime === +refEndDateTime) {
                newBeginDateTime = refBeginDateTime;

            } else {
                // Merge hole
                overwrite = false;
                if (+oldEndDateTime < +refBeginDateTime) {
                    newEndDateTime = refBeginDateTime;
                } else if (+oldBeginDateTime > +refEndDateTime) {
                    newBeginDateTime = refEndDateTime;
                }

                refBeginDateTime = -1;
                refEndDateTime = -1;
            }

            let update = () => {
                return new Promise((resolveUpdate, rejectUpdate) => {
                    db.run("UPDATE time SET beginDateTime = ?, endDateTime = ? WHERE (beginDateTime = ? AND endDateTime = ? OR beginDateTime = ? AND endDateTime = ?)",
                            newBeginDateTime, newEndDateTime, oldBeginDateTime, oldEndDateTime, refBeginDateTime, refEndDateTime, (err) => {
                                if (err) {
                                    rejectUpdate(err);
                                } else {
                                    module.exports.timeChange.emit("change", token, [newBeginDateTime, oldBeginDateTime, refBeginDateTime]);
                                    resolveUpdate(true);
                                }
                            });
                });
            };

            if (overwrite) {
                module.exports.removeOnDate({token, beginDateTime: refBeginDateTime, endDateTime: refEndDateTime})
                .then(update)
                .then(resolve)
                .catch(reject);

            } else {
                update()
                .then(() => {
                    return module.exports.mergeDuplicateTags({token, beginDateTime: newBeginDateTime, endDateTime: newEndDateTime});
                })
                .then(resolve)
                .catch(reject);
            }
        })
        .catch(reject);
    });
};

module.exports.getAll = ({token, beginDateTime = 0, endDateTime = Number.MAX_VALUE}) => {
    return new Promise((resolve, reject) => {
        userApi.isAuth({token})
        .then((db) => {
            db.all("SELECT * FROM time WHERE beginDateTime >= ? AND  beginDateTime <= ? ORDER BY beginDateTime DESC, endDateTime DESC",
                    beginDateTime, endDateTime, (err, rows) => {
                        if (err || !rows) {
                            reject(err);
                        } else {
                            resolve({rows});
                        }
                    });
        })
        .catch(reject);
    });
};

module.exports.getByEndDateTime = ({token, endDateTime}) => {
    return new Promise((resolve, reject) => {
        userApi.isAuth({token})
        .then((db) => {
            db.all("SELECT * FROM time WHERE endDateTime = ?", endDateTime, (err, rows) => {
                if (err || !rows) {
                    reject(err);
                } else {
                    resolve({rows});
                }
            });
        })
        .catch(reject);
    });
};

function getClauses(query) {
    if (!query.trim()) {
        return {request: "", parameters: []};
    }

    let request = " AND ( ";
    let separatorClause = "";
    let clauses = query.trim().split(" ");
    let parameters = [];

    clauses.forEach((clause) => {
        let expressions = clause.match(/(\+|-)?[^+-]+/g);

        let separatorExp = "";
        request += separatorClause + " (";

        expressions.forEach((exp) => {
            if (exp[0] === "-") {
                request += separatorExp + "NOT EXISTS (SELECT 1 FROM time a WHERE a.beginDateTime == t.beginDateTime AND a.endDateTime == t.endDateTime AND a.tag = ?) ";
                parameters.push(exp.substring(1));

            } else {
                request += separatorExp + "EXISTS (SELECT 1 FROM time a WHERE a.beginDateTime == t.beginDateTime AND a.endDateTime == t.endDateTime AND a.tag = ?) ";

                if (exp[0] === "+") {
                    parameters.push(exp.substring(1));
                } else {
                    parameters.push(exp);
                }
            }

            separatorExp = " AND ";
        });

        request += ") ";
        separatorClause = " OR ";
    });

    request += " ) ";

    return {request, parameters};
}

// +wid+dev-pause
// -pause
// +dev-wid
module.exports.getTotalTimeForReport = ({token, beginDateTime = 0, endDateTime = Number.MAX_VALUE, query = ""}) => {
    return new Promise((resolve, reject) => {
        userApi.isAuth({token})
        .then((db) => {
            let {request, parameters} = getClauses(query);

            db.get("SELECT sum(duration) as time FROM (" +
                    "SELECT (t.endDateTime - t.beginDateTime) as duration FROM time t " +
                    "WHERE t.beginDateTime >= ? AND t.beginDateTime <= ? " + request + " GROUP BY t.beginDateTime, t.endDateTime)",
                    +beginDateTime, +endDateTime, ...parameters, (err, row) => {
                        if (err || !row) {
                            reject(err);
                        } else {
                            resolve(row);
                        }
                    });
        })
        .catch(reject);
    });
};

module.exports.getTimeForReport = ({token, beginDateTime = 0, endDateTime = Number.MAX_VALUE, query = ""}) => {
    return new Promise((resolve, reject) => {
        userApi.isAuth({token})
        .then((db) => {
            let {request, parameters} = getClauses(query);

            db.all("SELECT group_concat(t.tag, ' ') as tags, t.beginDateTime, t.endDateTime, (t.endDateTime - t.beginDateTime) as duration FROM time t " +
                    "WHERE t.beginDateTime >= ? AND t.beginDateTime <= ? " + request + " GROUP BY t.beginDateTime, t.endDateTime",
                    +beginDateTime, +endDateTime, ...parameters, (err, rows) => {
                        if (err) {
                            reject(err);
                        } else {
                            resolve({rows});
                        }
                    });
        })
        .catch(reject);
    });
};

module.exports.getBeginDateTime = ({token}) => {
    return new Promise((resolve, reject) => {
        userApi.isAuth({token})
        .then((db) => {
            db.get("SELECT max(endDateTime) as beginDateTime FROM time", (err, row) => {
                if (err || !row) {
                    reject(err);
                } else {
                    resolve(row);
                }
            });
        })
        .catch(reject);
    });
};

module.exports.filter = ({token, beginDateTime = 0, endDateTime = Number.MAX_VALUE, query = "", includes = [], excludes = []}) => {
    let values = Array.isArray(includes) ? includes : [includes];
    let removes = Array.isArray(excludes) ? excludes : [excludes];

    let separator = "";
    return new Promise((resolve, reject) => {
        userApi.isAuth({token})
        .then((db) => {
            let requestFacet = "SELECT t.tag, count(t.tag) as count, sum(t.endDateTime - t.beginDateTime) as duration FROM time t " +
                    "WHERE t.beginDateTime >= ? AND t.beginDateTime <= ? ";

            if (values.length || removes.length) {
                requestFacet += " AND ( ";
            }

            removes.forEach(() => {
                requestFacet += separator + "NOT EXISTS (SELECT 1 FROM time a WHERE a.beginDateTime == t.beginDateTime AND a.endDateTime == t.endDateTime AND a.tag = ?) ";
                separator = "AND ";
            });
            // removes.forEach(() => {
            //     requestFacet += "AND tag != ? ";
            // });
            values.forEach(() => {
                requestFacet += separator + "EXISTS (SELECT 1 FROM time b WHERE b.beginDateTime == t.beginDateTime AND b.endDateTime == t.endDateTime AND b.tag = ?) ";
                separator = "AND ";
            });
            values.forEach(() => {
                requestFacet += "AND tag != ? ";
            });
            if (values.length || removes.length) {
                requestFacet += " ) ";
            }

            let {request, parameters} = getClauses(query);
            requestFacet += request;

            requestFacet += " GROUP BY t.tag ORDER BY duration DESC LIMIT 1";
            // console.log(requestFacet);

            db.get(requestFacet, +beginDateTime, +endDateTime, ...removes, ...values, ...values, ...parameters, (err, row) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(row);
                }
            });
        })
        .catch(reject);
    });
};

let _facet = ({token, beginDateTime = 0, endDateTime = Number.MAX_VALUE, query = "", includes = [], excludes = []}) => {
    return module.exports.filter({token, beginDateTime, endDateTime, query, includes, excludes})
    .then((value) => {
        if (value) {
            let newExcludes = excludes.concat([value.tag]);
            return _facet({token, beginDateTime, endDateTime, query, includes, excludes: newExcludes})
            .then((result) => {
                return Promise.resolve(result.concat([value]));
            });
        }

        return Promise.resolve([]);
    });
};

module.exports.facet = ({token, beginDateTime = 0, endDateTime = Number.MAX_VALUE, query = "", tags = []}) => {
    return _facet({token, beginDateTime, endDateTime, query, includes: tags})
    .then((result) => {
        return {rows: result.reverse()};
    });
};

let _facetExport = ({token, beginDateTime = 0, endDateTime = Number.MAX_VALUE, query = "", includes = [], excludes = [], result = {}}) => {
    return module.exports.filter({token, beginDateTime, endDateTime, query, includes, excludes})
    .then((value) => {
        if (value) {

            let nextResult = result[value.tag];
            if (!nextResult) {
                result[value.tag] = nextResult = {
                    __meta__: value
                };
            }

            return _facetExport({
                token,
                beginDateTime,
                endDateTime,
                query,
                includes: includes.concat(value.tag),
                excludes,
                result: nextResult
            })
            .then(() => {
                return _facetExport({
                    token,
                    beginDateTime,
                    endDateTime,
                    query,
                    includes: includes,
                    excludes: excludes.concat([value.tag]),
                    result
                });
            });
        }

        return Promise.resolve(result);
    });
};

module.exports.facetExport = ({token, beginDateTime = 0, endDateTime = Number.MAX_VALUE, query = "", tags = []}) => {
    return _facetExport({token, beginDateTime, endDateTime, query, includes: tags});
};

module.exports.getTags = ({token, beginDateTime = 0, endDateTime = Number.MAX_VALUE, query = ""}) => {
    return module.exports.facetExport({token, beginDateTime, endDateTime, query})
    .then((result) => {
        return {
            rows: result
        };
    });
};

module.exports.getStartWith = ({token, tag}) => {
    return new Promise((resolve, reject) => {
        userApi.isAuth({token})
        .then((db) => {
            db.all("SELECT DISTINCT tag FROM time WHERE tag LIKE ?", tag + "%", (err, rows) => {
                if (err) {
                    reject(err);
                } else {
                    resolve({rows});
                }
            });
        })
        .catch(reject);
    });
};

module.exports.completion = ({token, beginDateTime = 0, endDateTime = Number.MAX_VALUE, query = "", tags = []}) => {
    return module.exports.facet({token, beginDateTime, endDateTime, query, tags})
    .then((result) => {
        if (result.rows.length || !tags.length) {
            return result;
        }

        let values = Array.isArray(tags) ? tags : [tags];
        let startTag = values.pop();

        return module.exports.facet({token, beginDateTime, endDateTime, query, tags: values})
        .then((previousResult) => {
            previousResult.replace = true;
            previousResult.rows = previousResult.rows.filter((facet) => {
                return facet.tag.startsWith(startTag) && facet.tag !== startTag;
            });

            if (previousResult.rows.length) {
                return previousResult;
            }

            return module.exports.getStartWith({token, tag: startTag})
            .then((all) => {
                all.rows = all.rows.filter((facet) => {
                    return facet.tag !== startTag;
                });

                all.replace = true;
                return all;
            });
        });
    });
};

module.exports.updateTags = ({token, beginDateTime = 0, endDateTime = Number.MAX_VALUE, query = "", removed = [], added = []}) => {
    return module.exports.getTimeForReport({token, beginDateTime, endDateTime, query})
    .then(({rows}) => {
        let promises = [];

        dbHelper.beginTransaction(token)
        .then((db) => {
            let beginDateTimes = [];
            rows.forEach((row) => {
                let remove = Array.isArray(removed) ? removed : [removed];
                let add = Array.isArray(added) ? added : [added];

                beginDateTimes.push(row.beginDateTime);
                if (remove.length) {
                    let promise = module.exports.removeOnDateAndTag({token: db, beginDateTime: row.beginDateTime, endDateTime: row.endDateTime, tags: remove});
                    promises.push(promise);
                }
                if (add.length) {
                    let promise = module.exports.add({token: db, beginDateTime: row.beginDateTime, endDateTime: row.endDateTime, tags: add});
                    promises.push(promise);
                }
            });
            module.exports.timeChange.emit("change", token, beginDateTimes);

            return dbHelper.endTransaction(db, Promise.all(promises)
                    .then(() => {
                        return module.exports.mergeDuplicateTags({token: db, beginDateTime, endDateTime});
                    }));
        });
    });
};
