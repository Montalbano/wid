import {callApi, processResponse} from "./ActionUtils";

export let addIdentity = function(identifier) {
    callApi(`/api/identity/add?identifier=${identifier}`)
    .then(processResponse.bind(null, "ADD_IDENTITY", "IDENTITY_ERROR"));
};

export let removeIdentity = function(id) {
    callApi(`/api/identity/remove?id=${id}`)
    .then(processResponse.bind(null, {type: "REMOVE_IDENTITY", id}, "IDENTITY_ERROR"));
};

export let getIdentities = function() {
    callApi("/api/identities")
    .then(processResponse.bind(null, "GET_INDENTITIES", "IDENTITY_ERROR"));
};
