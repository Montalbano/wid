import {callApi, processResponse} from "./ActionUtils";
import {getActivities} from "./TimelineActions";
import {syncOfflineData} from "../utils/OfflineService";
import {connect, disconnect} from "../utils/SocketUtils";
import AppDispatcher from "../dispatcher/AppDispatcher";

export let signIn = function(username, passwd) {
    callApi(`/api/signin?username=${username}&passwd=${passwd}`)
    .then(processResponse.bind(null, "SIGNIN_DONE", "SIGNIN_ERROR"))
    .then(syncOfflineData)
    .then(getActivities)
    .then(connect);
};

export let signUp = function(username, email, passwd, confirm) {
    if (passwd !== confirm) {
        AppDispatcher.dispatch({
            type: "SIGNUP_ERROR_CONFIRM"
        });
        return;
    }

    callApi(`/api/signup?username=${username}&email=${email}&passwd=${passwd}`)
    .then(processResponse.bind(null, "SIGNUP_DONE", "SIGNUP_ERROR"));
};

export let signOut = function() {
    callApi("/api/signout")
    .then(processResponse.bind(null, "SIGNOUT_DONE", "SIGNOUT_ERROR"))
    .then(disconnect);
};

export let resetPasswd = function(email) {
    callApi(`/api/password/reset?email=${email}`)
    .then(processResponse.bind(null, "RESET_PASSWD_DONE", "RESET_PASSWD_ERROR"))
    .then(disconnect);
};

export let changePasswd = function(oldPasswd, newPasswd, confirm) {
    if (newPasswd !== confirm) {
        AppDispatcher.dispatch({
            type: "CHANGE_PASSWD_ERROR_CONFIRM"
        });
        return;
    }

    callApi(`/api/password/change?oldPasswd=${oldPasswd}&newPasswd=${newPasswd}`)
    .then(processResponse.bind(null, "CHANGE_PASSWD_DONE", "CHANGE_PASSWD_ERROR"));
};

export let checkUser = function() {
    return callApi("/api/user")
            .then((response) => {
                return response.json().then((result) => {
                    if (response.status === 200 || response.status === 0) {
                        AppDispatcher.dispatch(
                            Object.assign({type: "USER_LOGIN"}, result)
                        );
                        return Promise.resolve(result);
                    }

                    AppDispatcher.dispatch(
                        Object.assign({type: "USER_LOGOUT"}, result)
                    );
                    return Promise.reject(result);
                });
            });
};

export let verifyUser = function() {
    checkUser()
    .then(getActivities)
    .then(connect);
};

export let exportDb = function() {
    window.location = window.serverContext + "/api/export";
};
