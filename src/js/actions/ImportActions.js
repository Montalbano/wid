import {signOut} from "./UserActions";

export let importJtimer = function(file) {
    let formData = new FormData();
    formData.append("data", file);

    fetch(window.serverContext + "/api/import/jtimer", {
        credentials: "same-origin",
        method: "POST",
        body: formData
    }).then(signOut).catch(signOut);
};

export let importJson = function(file) {
    let formData = new FormData();
    formData.append("data", file);

    fetch(window.serverContext + "/api/import/json?drop=1", {
        credentials: "same-origin",
        method: "POST",
        body: formData
    }).then(signOut).catch(signOut);
};
