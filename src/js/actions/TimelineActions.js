import {callApi, processResponse, parametersToArray} from "./ActionUtils";
import moment from "moment";
import AppDispatcher from "../dispatcher/AppDispatcher";

export let addActivity = function(tag, beginDateTime, endDateTime = Date.now(), note) {
    AppDispatcher.dispatch({
        type: "PENDING"
    });

    let tags = parametersToArray("tags", tag);

    let request;
    if (beginDateTime) {
        request = `/api/time/add?${tags}beginDateTime=${beginDateTime}&endDateTime=${endDateTime}`;
    } else {
        request = `/api/time/add?${tags}endDateTime=${endDateTime}`;
    }

    if (note) {
        request += `&note=${note}`;
    }

    return callApi(request)
            .then(processResponse.bind(null, {type: "ADD_ACTIVITY", tag, beginDateTime, endDateTime}, "ACTIVITY_ERROR"));
};

export let getActivities = function() {
    let beginDateTime = moment().startOf("day");
    return callApi(`/api/times?beginDateTime=${beginDateTime}`)
            .then(processResponse.bind(null, "RECEIVE_ACTIVITIES", "ACTIVITY_ERROR"));
};

export let saveActivity = function(tag, beginDateTime, endDateTime, note) {
    AppDispatcher.dispatch({
        type: "PENDING"
    });

    let tags = parametersToArray("tags", tag);

    let request = `/api/time/save?${tags}beginDateTime=${beginDateTime}&endDateTime=${endDateTime}`;
    if (note) {
        request += `&note=${note}`;
    }

    callApi(request)
    .then(processResponse.bind(null, {type: "UPDATE_ACTIVITY", tag, beginDateTime, endDateTime}, "ACTIVITY_ERROR"));
};

export let modifyActivity = function(oldBeginDateTime, oldEndDateTime, newBeginDateTime, newEndDateTime) {
    AppDispatcher.dispatch({
        type: "PENDING"
    });

    callApi(`/api/time/modify?oldBeginDateTime=${oldBeginDateTime}&oldEndDateTime=${oldEndDateTime}&newBeginDateTime=${newBeginDateTime}&newEndDateTime=${newEndDateTime}`)
    .then(processResponse.bind(null, {type: "MODIFY_ACTIVITY", oldBeginDateTime, oldEndDateTime, newBeginDateTime, newEndDateTime}, "ACTIVITY_ERROR"));
};

export let mergeUpActivity = function(oldBeginDateTime, oldEndDateTime, overwrite) {
    AppDispatcher.dispatch({
        type: "PENDING"
    });

    callApi(`/api/time/merge/up?oldBeginDateTime=${oldBeginDateTime}&oldEndDateTime=${oldEndDateTime}` + (overwrite ? "&overwrite=true" : ""))
    .then(processResponse.bind(null, {type: "MERGE_UP_ACTIVITY", oldBeginDateTime, oldEndDateTime, overwrite}, "ACTIVITY_ERROR"));
};

export let mergeDownActivity = function(oldBeginDateTime, oldEndDateTime, overwrite) {
    AppDispatcher.dispatch({
        type: "PENDING"
    });

    callApi(`/api/time/merge/down?oldBeginDateTime=${oldBeginDateTime}&oldEndDateTime=${oldEndDateTime}` + (overwrite ? "&overwrite=true" : ""))
    .then(processResponse.bind(null, {type: "MERGE_DOWN_ACTIVITY", oldBeginDateTime, oldEndDateTime, overwrite}, "ACTIVITY_ERROR"));
};

export let changeFilter = function(value) {
    let beginDateTime = moment();
    if (value === 0) {
        beginDateTime.startOf("day");

    } else if (value === 1) {
        beginDateTime.startOf("week");

    } else if (value === 2) {
        beginDateTime.startOf("month");

    } else if (value === 3) {
        beginDateTime.startOf("year");

    }
    callApi(`/api/times?beginDateTime=${beginDateTime}`)
    .then(processResponse.bind(null, {type: "FILTER_CHANGE", value}, "ACTIVITY_ERROR"));
};

export let facetFilter = function(tag = []) {
    let includes = tag.filter((item) => {return item !== "";});
    let tags = parametersToArray("tags", includes);

    callApi(`/api/time/facet?${tags}beginDateTime=0`)
    .then(processResponse.bind(null, {type: "GET_FACETS", tag}, "FACET_ERROR"));
};

export let completion = function(tag = []) {
    let includes = tag.filter((item) => {return item !== "";});
    let tags = parametersToArray("tags", includes);
    let beginDateTime = Date.now() - 366 * 24 * 60 * 60 * 1000;

    callApi(`/api/time/completion?${tags}beginDateTime=${beginDateTime}`)
    .then(processResponse.bind(null, {type: "GET_FACETS", tag}, "FACET_ERROR"));
};
