import {callApi, processResponse, parametersToArray} from "./ActionUtils";

export let search = function(query, beginDateTime, endDateTime) {
    let request = `/api/time/tags?query=${encodeURIComponent(query)}`;

    if (beginDateTime) {
        let value = new Date(beginDateTime);
        request += `&beginDateTime=${value.getTime()}`;
    }
    if (endDateTime) {
        let value = new Date(endDateTime);
        request += `&endDateTime=${value.getTime()}`;
    }

    callApi(request)
    .then(processResponse.bind(null, "GET_ROWS", "TAGS_ERROR"));
};

export let update = function(query, beginDateTime, endDateTime, removed = [], added = []) {
    let request = `/api/time/tags/update?query=${encodeURIComponent(query)}`;

    if (beginDateTime) {
        let value = new Date(beginDateTime);
        request += `&beginDateTime=${value.getTime()}`;
    }
    if (endDateTime) {
        let value = new Date(endDateTime);
        request += `&endDateTime=${value.getTime()}`;
    }

    if (removed && removed.length) {
        request += "&" + parametersToArray("removed", removed);
    }
    if (added && added.length) {
        request += "&" + parametersToArray("added", added);
    }

    callApi(request)
    .then(processResponse.bind(null, "UPDATE_ROWS", "TAGS_ERROR"));
};
