import AppDispatcher from "../dispatcher/AppDispatcher";
import {fetchOffline} from "../utils/OfflineService";

export let callApi = (url) => {
    if (!window.online) {
        return fetchOffline(url);
    }

    return fetch(window.serverContext + url, {
        credentials: "same-origin"
    });
};

export let parametersToArray = (name, values) => {
    let result = values.reduce((prev, value) => {
        return prev + name + "=" + encodeURIComponent(value) + "&";
    }, "");

    return result;
};

export let processResponse = (done, error, response) => {
    let json = response.json();

    if (response.status === 200 || response.status === 0) {
        return json.then((result) => {
            if (typeof done === "string") {
                done = {type: done};
            }

            AppDispatcher.dispatch(
                Object.assign(done, result)
            );
            return result;
        });

    } else if (response.status === 401) {
        return json.then((err) => {
            AppDispatcher.dispatch(
                Object.assign({type: "SIGNOUT_DONE"}, err)
            );
            return Promise.reject(err);
        });
    }

    return json.then((err) => {
        if (typeof error === "string") {
            error = {type: error};
        }

        AppDispatcher.dispatch(
            Object.assign(error, err)
        );
        return Promise.reject(err);
    });
};
