import {callApi, processResponse} from "./ActionUtils";

export let generateKey = function(name) {
    callApi(`/api/key/add?name=${name}`)
    .then(processResponse.bind(null, "ADD_KEY", "APPLICATION_ERROR"));
};

export let removeKey = function(id) {
    callApi(`/api/key/remove?id=${id}`)
    .then(processResponse.bind(null, {type: "REMOVE_KEY", id}, "APPLICATION_ERROR"));
};

export let getKeys = function() {
    callApi("/api/keys")
    .then(processResponse.bind(null, "GET_KEYS", "APPLICATION_ERROR"));
};
