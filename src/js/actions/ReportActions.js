import {callApi, processResponse, parametersToArray} from "./ActionUtils";

export let addReport = function(name, tags, period, widget) {
    callApi(`/api/report/add?name=${name}&tags=${encodeURIComponent(tags)}&period=${period}&widget=${widget}`)
    .then(processResponse.bind(null, {type: "ADD_REPORT", name, tags, period, widget}, "REPORT_ERROR"));
};

export let removeReport = function(id) {
    callApi(`/api/report/remove?id=${id}`)
    .then(processResponse.bind(null, {type: "REMOVE_REPORT", id}, "REPORT_ERROR"));
};

export let getReports = function() {
    callApi("/api/reports")
    .then(processResponse.bind(null, "GET_REPORTS", "REPORT_ERROR"));
};

export let getWidgets = function(beginDateTime, endDateTime) {
    callApi(`/api/report/widgets?beginDateTime=${beginDateTime}&endDateTime=${endDateTime}`)
    .then(processResponse.bind(null, "GET_WIDGETS", "REPORT_ERROR"));
};

export let getDetails = function(id, beginDateTime, endDateTime, tag = []) {
    let tags = parametersToArray("tags", tag);

    callApi(`/api/report/details?${tags}id=${id}&beginDateTime=${beginDateTime}&endDateTime=${endDateTime}`)
    .then(processResponse.bind(null, {type: "GET_DETAILS", id, date: beginDateTime}, "REPORT_ERROR"));
};
