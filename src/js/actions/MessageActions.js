import AppDispatcher from "../dispatcher/AppDispatcher";

export let clearMessage = () => {
    if (!AppDispatcher.isDispatching()) {
        AppDispatcher.dispatch({
            type: "CLEAR_MESSAGE"
        });
    }
};
