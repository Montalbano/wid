import {callApi, processResponse} from "./ActionUtils";
import AppDispatcher from "../dispatcher/AppDispatcher";

export let eventUpdate = (events) => {
    AppDispatcher.dispatch({
        type: "UPDATE_EVENTS",
        events
    });
};

export let getEvents = (beginDateTime, endDateTime) => {
    callApi(`/api/events?beginDateTime=${beginDateTime}&endDateTime=${endDateTime}`)
    .then(processResponse.bind(null, {type: "GET_EVENTS", beginDateTime, endDateTime}, "EVENTS_ERROR"));
};

export let removeEvent = (id) => {
    callApi(`/api/event/remove?id=${id}`)
    .then(processResponse.bind(null, {type: "REMOVE_EVENT", id}, "EVENTS_ERROR"));
};
