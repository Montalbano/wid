/* global chrome*/

let serverUrl;
let idleTime;

chrome.storage.sync.get({
    serverUrl: "https://wid.chorem.com",
    idleTime: 5

}, function(items) {
    idleTime = +items.idleTime;
    serverUrl = items.serverUrl;

    chrome.idle.setDetectionInterval(60 * idleTime); //seconds
    chrome.idle.onStateChanged.addListener(function(state) {
        let url = serverUrl + "/api/event/start?source=EXTENSION&value=idle&shift=" + (idleTime * 60 * 1000);
        if (state === "active") {
            url = serverUrl + "/api/event/end?source=EXTENSION";
        }

        fetch(url, {credentials: "include"});
    });
});

chrome.storage.onChanged.addListener(function(changes) {
    for (let key in changes) {
        if ({}.hasOwnProperty.call(changes, key)) {

            let storageChange = changes[key];
            if (key === "serverUrl") {
                serverUrl = storageChange.newValue;

            } else if (key === "idleTime") {
                idleTime = +storageChange.newValue;
                chrome.idle.setDetectionInterval(60 * idleTime); //seconds
            }
        }
    }
});
