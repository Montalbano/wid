/* global chrome*/

function saveOptions() {
    let serverUrl = document.getElementById("serverUrl").value;
    let idleTime = document.getElementById("idleTime").value;

    chrome.storage.sync.set({
        serverUrl,
        idleTime
    }, function() {
        let status = document.getElementById("status");
        status.textContent = "Options saved.";
        setTimeout(function() {
            status.textContent = "";
        }, 750);
    });
}

function restoreOptions() {
    chrome.storage.sync.get({
        serverUrl: "http://localhost:9090",
        idleTime: 5
    }, function(items) {
        document.getElementById("serverUrl").value = items.serverUrl;
        document.getElementById("idleTime").value = items.idleTime;
    });
}

document.addEventListener("DOMContentLoaded", restoreOptions);
document.getElementById("save").addEventListener("click", saveOptions);
